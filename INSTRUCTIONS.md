# Instructions

This client does not provide instructions to the user. Thus, instructions are
provided here.

## Splash screen

Enter your player name into the text input labelled `Who are you?`, and enter
the name of the game you want to play into the text input labelled `Where are
you going?`.

Press the button below (usually labelled `Go!`).

## Game Lobby Screen

Wait for your opponent to join the game. Once there are two players in the
game lobby, either player can start the game by pressing the button. (Usually
labelled `Go!` or `Start`).

## Setup Screen

The list at the left hand side of the screen is a list of ships that you can
place. The ship whose name is in bold is the ship that you are currently
placing.

Mousing over the grid in the center of the screen will display a preview of
the ship. If the ship cannot be placed where its preview is, the preview will
be red.

Left click to place the ship. Right click to change the direction of the ship.
The next unplaced ship will automatically be selected when you place a ship,
and once all ships are placed, none will be selected. Clicking on the name of
a ship will allow you to place that ship, regardless of the normal order of
ship placement. Ships that have already been placed may be placed again on
a different part of the board.

When you are done placing ships, click the `Done` button at the bottom of the
left sidebar. When your opponent also places their ships, the game will start.

## Gameplay Screens

There are three elements to the gameplay screen: A list of ships in on the
sidebar, the main display in the center of the screen, and the secondary
display at the bottom of the sidebar.

Your tracking grid displays the results of your shots. Hits are shown in `red`
and misses are shown in `slategrey`. When your turn begins, your tracking
grid is automatically moved to the main display. Clicking on the secondary
display swaps the contents of the main and secondary displays.

Your board displays your ships and records the results of your opponent's
shots. Hits are shown in `red`, and misses are shown in `slategrey`. When your
turn ends, your board is automatically moved to the main display. Clicking on
the secondary display swaps the contents of the main and secondary displays.

When the main display contains your board, the list of ships is a list of your
ships. When the main display contains your tracking grid, the list of ships is
a list of your opponent's ships. The names of ships that have been sunk are
greyed out.

