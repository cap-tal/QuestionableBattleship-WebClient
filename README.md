# QB-WebClient (Simple)

This is a simple, near-minimal web browser based client for the classic
variation of [Questionable Battleship](https://www.questionablebattleship.com).

Beware:
* This client is not responsive and was developed with a laptop sized 16:9
  display in mind.
* This client was developed with mouse input in mind.
* This client was developed primarily as a development tool.

## Who should use this client?

In general, prefer the full client to this simple client.

1. You are playing on older or less capable hardware that is stressed by the
   full client.
2. People playing from a device which has limited battery life, or who wish to
   conserve power.
3. People who don't have time for animations of any kind.

## Who should not use this client?

1. People playing on reasonably modern hardware.
2. People on mobile devices like smartphones or small tablets should avoid
   using this client when possible.
3. People morally opposed to the &lt;canvas&gt; tag.
4. People who hate [jQuery](https://jquery.com/) with a burning passion.

## Instructions

See [instructions](INSTRUCTIONS.md).

