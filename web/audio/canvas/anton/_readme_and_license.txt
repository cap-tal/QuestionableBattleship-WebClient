Sound pack downloaded from Freesound.org
----------------------------------------

This pack of sounds contains sounds by the following user:
 - Anton ( https://freesound.org/people/Anton/ )

You can find this pack online at: https://freesound.org/people/Anton/packs/4/

License details
---------------

Attribution: http://creativecommons.org/licenses/by/3.0/


Sounds in this pack
-------------------

  * 49__Anton__Glass_G_mf.wav
    * url: https://freesound.org/s/49/
    * license: Attribution
  * 52__Anton__Glass_G_mf.wav
    * url: https://freesound.org/s/52/
    * license: Attribution
  * 50__Anton__Glass_G_pp.wav
    * url: https://freesound.org/s/50/
    * license: Attribution
  * 48__Anton__Glass_G_ff.wav
    * url: https://freesound.org/s/48/
    * license: Attribution
  * 51__Anton__Glass_G_ff.wav
    * url: https://freesound.org/s/51/
    * license: Attribution
  * 53__Anton__Glass_G_pp.wav
    * url: https://freesound.org/s/53/
    * license: Attribution
  * 37__Anton__Glass_E0_mf.wav
    * url: https://freesound.org/s/37/
    * license: Attribution
  * 43__Anton__Glass_F_mf.wav
    * url: https://freesound.org/s/43/
    * license: Attribution
  * 39__Anton__Glass_E1_ff.wav
    * url: https://freesound.org/s/39/
    * license: Attribution
  * 47__Anton__Glass_F_pp.wav
    * url: https://freesound.org/s/47/
    * license: Attribution
  * 46__Anton__Glass_F_mf.wav
    * url: https://freesound.org/s/46/
    * license: Attribution
  * 45__Anton__Glass_F_ff.wav
    * url: https://freesound.org/s/45/
    * license: Attribution
  * 42__Anton__Glass_F_ff.wav
    * url: https://freesound.org/s/42/
    * license: Attribution
  * 40__Anton__Glass_E1_mf.wav
    * url: https://freesound.org/s/40/
    * license: Attribution
  * 44__Anton__Glass_F_pp.wav
    * url: https://freesound.org/s/44/
    * license: Attribution
  * 41__Anton__Glass_E1_pp.wav
    * url: https://freesound.org/s/41/
    * license: Attribution
  * 38__Anton__Glass_E0_pp.wav
    * url: https://freesound.org/s/38/
    * license: Attribution
  * 28__Anton__Glass_C_mf.wav
    * url: https://freesound.org/s/28/
    * license: Attribution
  * 27__Anton__Glass_C_ff.wav
    * url: https://freesound.org/s/27/
    * license: Attribution
  * 30__Anton__Glass_D_ff.wav
    * url: https://freesound.org/s/30/
    * license: Attribution
  * 36__Anton__Glass_E0_ff.wav
    * url: https://freesound.org/s/36/
    * license: Attribution
  * 31__Anton__Glass_D_mf.wav
    * url: https://freesound.org/s/31/
    * license: Attribution
  * 33__Anton__Glass_D_ff.wav
    * url: https://freesound.org/s/33/
    * license: Attribution
  * 34__Anton__Glass_D_mf.wav
    * url: https://freesound.org/s/34/
    * license: Attribution
  * 26__Anton__Glass_C_pp.wav
    * url: https://freesound.org/s/26/
    * license: Attribution
  * 29__Anton__Glass_C_pp.wav
    * url: https://freesound.org/s/29/
    * license: Attribution
  * 25__Anton__Glass_C_mf.wav
    * url: https://freesound.org/s/25/
    * license: Attribution
  * 35__Anton__Glass_D_pp.wav
    * url: https://freesound.org/s/35/
    * license: Attribution
  * 32__Anton__Glass_D_pp.wav
    * url: https://freesound.org/s/32/
    * license: Attribution
  * 18__Anton__Glass_A_ff.wav
    * url: https://freesound.org/s/18/
    * license: Attribution
  * 17__Anton__Glass_A_pp.wav
    * url: https://freesound.org/s/17/
    * license: Attribution
  * 22__Anton__Glass_B_mf.wav
    * url: https://freesound.org/s/22/
    * license: Attribution
  * 20__Anton__Glass_A_pp.wav
    * url: https://freesound.org/s/20/
    * license: Attribution
  * 23__Anton__Glass_B_pp.wav
    * url: https://freesound.org/s/23/
    * license: Attribution
  * 19__Anton__Glass_A_mf.wav
    * url: https://freesound.org/s/19/
    * license: Attribution
  * 16__Anton__Glass_A_mf.wav
    * url: https://freesound.org/s/16/
    * license: Attribution
  * 21__Anton__Glass_B_ff.wav
    * url: https://freesound.org/s/21/
    * license: Attribution
  * 24__Anton__Glass_C_ff.wav
    * url: https://freesound.org/s/24/
    * license: Attribution
  * 15__Anton__Glass_A_ff.wav
    * url: https://freesound.org/s/15/
    * license: Attribution


