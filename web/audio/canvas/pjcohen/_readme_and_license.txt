Sound pack downloaded from Freesound.org
----------------------------------------

This pack of sounds contains sounds by the following user:
 - pjcohen ( https://freesound.org/people/pjcohen/ )

You can find this pack online at: https://freesound.org/people/pjcohen/packs/2902/

License details
---------------

Creative Commons 0: http://creativecommons.org/publicdomain/zero/1.0/
Attribution: http://creativecommons.org/licenses/by/3.0/


Sounds in this pack
-------------------

  * 147039__pjcohen__rivetsinstantsizzlecymbal.wav
    * url: https://freesound.org/s/147039/
    * license: Attribution
  * 46647__pjcohen__Zildjian_8_inch_K_Custom_Dark_Splash_Cymbal.wav
    * url: https://freesound.org/s/46647/
    * license: Creative Commons 0
  * 46646__pjcohen__Zildjian_8_inch_K_Custom_Dark_Splash_Cymbal_Bell.wav
    * url: https://freesound.org/s/46646/
    * license: Creative Commons 0
  * 46567__pjcohen__Ludwig_Black_Beauty_Snare_Ghost_Notes_Unprocessed.wav
    * url: https://freesound.org/s/46567/
    * license: Creative Commons 0
  * 46566__pjcohen__Ludwig_Black_Beauty_Snare_Drum_Unprocessed.wav
    * url: https://freesound.org/s/46566/
    * license: Creative Commons 0
  * 46565__pjcohen__Ludwig_Black_Beauty_Snare_Drum_Open_Rimshot_Unprocessed.wav
    * url: https://freesound.org/s/46565/
    * license: Creative Commons 0
  * 46564__pjcohen__Ludwig_Black_Beauty_Snare_Drum_Closed_Rimshot_Unprocessed.wav
    * url: https://freesound.org/s/46564/
    * license: Creative Commons 0
  * 46533__pjcohen__Zildjian_Transitional_Stamp_Sizzle_Ride_Cymbal_Bell_Hit_longer_fade.wav
    * url: https://freesound.org/s/46533/
    * license: Creative Commons 0
  * 46532__pjcohen__Stereo_Processed_Snare_Drum_4.wav
    * url: https://freesound.org/s/46532/
    * license: Creative Commons 0
  * 46531__pjcohen__Stereo_Processed_Snare_Drum_3_.wav
    * url: https://freesound.org/s/46531/
    * license: Creative Commons 0
  * 46530__pjcohen__Stereo_Processed_Snare_Drum_2.wav
    * url: https://freesound.org/s/46530/
    * license: Creative Commons 0
  * 46529__pjcohen__Stereo_Processed_Snare_Drum_1_.wav
    * url: https://freesound.org/s/46529/
    * license: Creative Commons 0
  * 46528__pjcohen__Stereo_Processed_Bass_Kick_Drum_2_.wav
    * url: https://freesound.org/s/46528/
    * license: Creative Commons 0
  * 46527__pjcohen__Stereo_Processed_Bass_Kick_Drum_1_.wav
    * url: https://freesound.org/s/46527/
    * license: Creative Commons 0
  * 46446__pjcohen__Zildjian_Transitional_Stamp_Sizzle_Ride_Cymbal_Bell_Hit.wav
    * url: https://freesound.org/s/46446/
    * license: Creative Commons 0
  * 45669__pjcohen__Zildjian_Transitional_Stamp_Sizzle_Ride_Cymbal_Hit.wav
    * url: https://freesound.org/s/45669/
    * license: Creative Commons 0
  * 45668__pjcohen__Zildjian_A_Custom_Hi_Hat_Cymbals_Pedal_Chic.wav
    * url: https://freesound.org/s/45668/
    * license: Creative Commons 0
  * 45667__pjcohen__Zildjian_A_Custom_Hi_Hat_Cymbals_Open_Hit.wav
    * url: https://freesound.org/s/45667/
    * license: Creative Commons 0
  * 45666__pjcohen__Zildjian_A_Custom_Hi_Hat_Cymbals_Loose_Hit.wav
    * url: https://freesound.org/s/45666/
    * license: Creative Commons 0
  * 45665__pjcohen__Zildjian_A_Custom_Hi_Hat_Cymbals_Foot_Splash.wav
    * url: https://freesound.org/s/45665/
    * license: Creative Commons 0
  * 45664__pjcohen__Zildjian_A_Custom_Hi_Hat_Cymbals_Closed_Hit.wav
    * url: https://freesound.org/s/45664/
    * license: Creative Commons 0


