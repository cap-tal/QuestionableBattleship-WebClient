window.addEventListener("load", game);

/** todo
 * 
 * music?? maybe
 * transitions
 * implement start_attack
 * change audio arrays to audioStack objects
 * 
**/



function game() {
    console.log("loading...");
    
    var transitionToggle = false;
    
    
    
    
    // game data
    var game_name, game_id, player_name, player_id, opponent_id, turn;
    
    // game timing
    var FPS, maxLoadTime;
    
    // canvas
    var canvas, context;
    
    // objects
    var players, elements, communicator;
    
    var currentGameState;
            
    var frame, windowToCanvasRatio, overflowSize, overflowRatio, canvasMovesWithScroll, block_count, dimensions, ORIGINAL_GRID_PADDING, UNORIGINAL_GRID_PADDING, 
    grid_padding, grid_size, block_size, peg_size, block_padding, pointDirection, colorScheme;
    
    var explosionRadius, shipHP;
    
    // particle variables
    var fractures, particleFramesOfExistence, forceScaling, forceRandomizationFactor, dragForce, dragForceLifespanScalingDivider;
    
    // transition variables
    var currentlyTransitioning, framesPerBlock, foldIndentRatio;
    
    // menu variables
    var bulletLength, bulletWidth;
    
    var mouseX, mouseY;
    
    var ball;
    var transition;
    var menu;
    var crosshair;
    
    var loopInterval;
    
    var isLandscape;
    
    var time;
    
    var winner_id;
    
    var sideShift = 0;
    
    /*
    var newElement = document.createElement("textarea");
    newElement.id = "stateInput";
    document.getElementById('canvasContainer').children[0].appendChild(newElement);
    
    
    newElement = document.createElement("textarea");
    newElement.id = "particleCount";
    document.getElementById('canvasContainer').children[0].appendChild(newElement);
    
    newElement = document.createElement("button");
    newElement.innerHTML = "win";
    document.getElementById('optionsContainer').children[0].appendChild(newElement);
    
    newElement.onclick = function() {
        currentGameState = "endgame";
        winner_id = player_id;
        randomStuff.clear();
    };
    */
    
    var chat = new Chat();
    function Chat() {
        var chatDiv;
        var chatText;
        var entryField;
        var entryButton;
        var activated = false;
        
        this.activate = function() {
            chatDiv = document.createElement("div");
            chatDiv.id = "chatDiv";
            chatDiv.style.position = "absolute";
            chatDiv.style.resize = "none";
            document.getElementById("canvasContainer").children[0].appendChild(chatDiv);
            
            entryField = document.createElement("textarea");
            entryField.id = "entryField";
            if (isMobile) {
                entryField.style.fontSize = "64px";
                entryField.style.height = "128px";
            }
            chatDiv.appendChild(entryField);
            
            entryButton = document.createElement("button");
            entryButton.id = "entryButton";
            entryButton.innerHTML = "Send";
            
            if (isMobile) {
                entryButton.style.height = "123px";
            }
            
            entryButton.addEventListener("click", this.receiveMessage);
            chatDiv.appendChild(entryButton);
            
            chatText = document.createElement("div");
            chatText.id = "chatText";
            chatDiv.appendChild(chatText);
            
            activated = true;
            this.update();
        };
        this.deactivate = function() {
            try {
                chatDiv.parentElement.removeChild(chatDiv);
            } catch (error) {
                console.log(error);
            }
        };
        
        
        
        this.receiveMessage = function(event) {
            var newLine = document.createElement("p");
            var chatText = document.getElementById("chatText");
            chatText.appendChild(newLine);
            var field = event.target.parentElement.childNodes;
            for (var i = 0; i < field.length; i++) {
                if (field[i].id == "entryField") {
                    field = field[i];
                }
            }
            newLine.innerHTML = player_name + ": " + field.value;
            
            chatText.scrollTop = parseInt(chatText.scrollHeight, 10);
            document.getElementById("entryField").focus();
            
        };
        
        this.update = function() {
            if (!activated) return;
            chatDiv.style.top = (isLandscape ? Math.min(dimensions / 2, overflowSize) : dimensions) + "px";
            chatDiv.style.left = (isLandscape ? dimensions : Math.min(dimensions / 2, overflowSize)) + "px";
            chatDiv.style.height = (isLandscape ? dimensions - Math.min(overflowSize, dimensions / 2): overflowSize) + "px";
            chatDiv.style.width = (isLandscape ? overflowSize : dimensions - Math.min(overflowSize , dimensions / 2)) + "px";
            
            if (parseInt(chatDiv.style.width.slice(0, -2), 10) < 160) {
                entryButton.innerHTML = "";
            } else {
                entryButton.innerHTML = "Send";
            }
            
            
        
            if (overflowSize < 10) {
                chatText.style.top = "-10px";
                chatText.style.height = "0px";
            } else {
                chatText.style.top = "10px";
                chatText.style.height = Math.max(0, (isLandscape ? dimensions - Math.min(overflowSize, dimensions / 2): overflowSize) - (isMobile ? 150 : 50)) + "px";
                
            }
            
        };
    }
    
    
    var isMobile;
    
    
    var randomStuff = new Pieces;
    
    
    function Pieces() {
        var pieces = [];
        
        this.clear = function() {
            pieces = [];
        };
        
        this.addRandomSpeck = function() {
            if (sideShift) {
                this.addCircle(
                    (Math.random() + 1) * (dimensions + (isLandscape ? overflowSize : 0)), 
                    Math.random() * (dimensions + (isLandscape ? 0 : overflowSize)),
                    block_size * Math.random(),
                    1,
                    true
                );
            } else {
                this.addCircle(
                    Math.random() * (dimensions + (isLandscape ? overflowSize : 0)), 
                    (Math.random() + 1) * (dimensions + (isLandscape ? 0 : overflowSize)),
                    block_size * Math.random(),
                    1,
                    true
                );
            }
        };
        
        this.addCircle = function(centerX, centerY, size, fractures, underwater) {
            var color;
            if (underwater) {
                color = "#DCDCDC";
            } else {
                color = colorScheme[Math.floor(colorScheme.length * Math.random())];
            }
            if (fractures > 1) {
                for(var i = 0; i < fractures; i++) {
                    for (var ii = 0; ii < fractures; ii++) {
                        
                        if (
                            Math.sqrt(Math.pow((fractures / 2) - (i + .5), 2) + Math.pow((fractures / 2) - (ii + .5), 2)) <= (fractures / 2) 
                        ) {
                            this.addPiece(
                                {
                                    "x" : (centerX - (size / 2)) + (size / fractures * i), 
                                    "y" : (centerY - (size / 2)) + (size / fractures * ii)
                                },
                                underwater ? 0 : 50, 
                                {"x" : centerX / dimensions, "y" : centerY / dimensions}, 
                                size / fractures, 
                                color,
                                underwater,
                                size / block_size
                            );
                        }
                    }
                }
            } else {
                this.addPiece(
                    {
                        "x" : centerX, 
                        "y" : centerY
                    },
                    0, 
                    {"x" : centerX / dimensions, "y" : centerY / dimensions}, 
                    size, 
                    color,
                    underwater,
                    size / block_size
                    );
            }
        };
        
        this.applyForce = function(coordinates) {
            for (var i = 0; i < pieces.length; i++) {
                pieces[i].applyForce(50, coordinates);
            }
        };
        
        this.addPiece = function(coordinates, force, forceSource, size, color, underwater, distance) {
            pieces.push(new Piece(coordinates, force, forceSource, size, color, underwater, distance));
        };
        
        this.update = function() {
            for (var i = 0; i < pieces.length; i++) {
                pieces[i].update();
            }
        };
        
        function Piece(coordinates, force, forceSource, size, color, underwater, distance) {
            this.x = coordinates.x / dimensions;                                                // x at start
            this.y = coordinates.y / dimensions;                                                // y at start
            this.size = size;                                                       // size of square particle
            this.color = color;                                                     // color is inherited from original block
            this.lifespan = 0;                                                      // lifespan is increased by 1 every frame
            this.underwater = underwater;
            this.distance = distance;
            this.vx = 0;
            this.vy = 0;
            this.mass = size;
            
            this.centerX = this.x + (this.size / dimensions / 2);
            this.centerY = this.y + (this.size / dimensions / 2);
            
            this.applyForce = function(force, source) {
                // determines slope of f, forceorce 
                var slope = (this.centerY - source.y) / (this.centerX - source.x);
        
                // gets the force angle from the slope: tan-1(m) = theta
                var forceAngle = Math.atan(Math.abs(slope));
                
                // adjusts for negative forces and stuff because tan-1 works best with angles in quadrant I (also less confusing)
                var forceX = this.centerX < source.x ? -force : force;
                var forceY = this.centerY < source.y ? -force : force;
                
                
                // scales force as determined by forceScaling basically
                forceX /= forceScaling;
                forceY /= forceScaling;
                
                
                this.vx += forceX * Math.cos(forceAngle) * (forceRandomizationFactor * Math.random() + (1 - forceRandomizationFactor)) / dimensions;
                this.vy += forceY * Math.sin(forceAngle) * (forceRandomizationFactor * Math.random() + (1 - forceRandomizationFactor)) / dimensions;
            
            };
            this.applyForce(force, forceSource);
            
            
            // updates this particle: checks for collisions, updates location, renders and decays the particle
            this.update = function() {
                
                this.centerX = this.x + (this.size / dimensions / 2);
                this.centerY = this.y + (this.size / dimensions / 2);
                
                if (this.underwater) {
                    if (players.count() < 2) {
                        this.vx = -sideShift;
                    } else {
                        this.vx -= (sideShift / 4);
                    }
                    this.vy -= .000005 * this.lifespan;// * Math.random();
                    this.vx -= .000001 * this.lifespan * ((Math.random() - .5));
                    
                    this.x += this.vx * distance;
                    this.y += (this.vy - (loadPercentage.opacity * .05)) * this.distance;  
                    
                    this.vx /= Math.pow(dragForce, this.lifespan / dragForceLifespanScalingDivider);
                    this.vy /= Math.pow(dragForce, this.lifespan / dragForceLifespanScalingDivider);
                } else {
                    this.checkCollision();
                    this.vy += .000001 * this.lifespan;
                    
                    this.x += this.vx;
                    this.y += this.vy;  
                    
                    this.vx *= .95;
                    this.vy *= .95;
                    
                }
                
                
                
                
                if (
                    (this.x > 0 || this.x < ((dimensions + (isLandscape ? overflowSize : 0) / dimensions))) && 
                    (this.y > 0 || this.y < ((dimensions + (isLandscape ? 0 : overflowSize) / dimensions)))
                ) {
                        
                    context.fillStyle = ("rgba(" + parseInt(this.color.slice(1, 3), 16) + ", " + parseInt(this.color.slice(3, 5), 16) + ", " + parseInt(this.color.slice(5, 7), 16) + ", " + ((particleFramesOfExistence - this.lifespan + 20) / particleFramesOfExistence) + ")");
                    context.fillRect(
                        this.x * dimensions, 
                        this.y * dimensions, 
                        this.size, 
                        this.size);
                        
                        /*
                        context.beginPath();
                        context.strokeStyle = ("rgba(" + parseInt(this.color.slice(1, 3), 16) + ", " + parseInt(this.color.slice(3, 5), 16) + ", " + parseInt(this.color.slice(5, 7), 16) + ", " + ((particleFramesOfExistence - this.lifespan + 20) / particleFramesOfExistence) + ")");
                        context.lineWidth = 1;
                        
                        context.arc(this.x * dimensions, this.y * dimensions, this.size, 0, Math.PI*2, true);
                        context.stroke();
                        
                        context.moveTo(this.x * dimensions, this.y * dimensions);
                        context.lineTo((this.x + (this.vx * 20 * distance)) * dimensions, (this.y + (this.vy * 20 * distance)) * dimensions);
                        
                        context.stroke();
                        context.closePath();
                        */
                }
                
                    
                if (this.underwater) {
                    if (sideShift > .25) {
                        this.lifespan += 5;
                    } else {
                        this.lifespan += .5;
                    }
                } else if (currentGameState == "endgame") {
                    this.lifespan += 5;
                } else {
                    this.lifespan++;
                }
                if (this.lifespan > (particleFramesOfExistence)) {
                    var index = pieces.indexOf(this);
                    pieces.splice(index, 1);
                }
            };
            this.checkCollision = function() {
                // left side
                if (this.x < 0) {
                    this.vx *= -.5;
                    this.x = 0;
                }
                if (this.x > ((dimensions + (isLandscape ? overflowSize : 0)) / dimensions)) {
                    this.vx *= -.5;
                    this.x = ((dimensions + (isLandscape ? overflowSize : 0)) / dimensions);
                }
                if (this.y < 0) {
                    this.vy *= -.5;
                    this.y = 0;
                }
                if (this.y > ((dimensions + (isLandscape ? 0 : overflowSize)) / dimensions) - .01) {
                    this.vy *= -.5;
                    this.y = ((dimensions + (isLandscape ? 0 : overflowSize)) / dimensions) - .01;
                }
            };
        }
    }
    
    function initialize() {
        players = new GamePlayers();
        turn = false;
        
        FPS = 50;
        maxLoadTime = 3 * FPS;
        
        canvas = document.getElementById("gameCanvas");
        context = canvas.getContext('2d');
        
        elements = new HTMLElements();
        communicator = new Communications();
        
            // statefulness
        currentGameState = 'initialization';
        
        frame = 0;
        windowToCanvasRatio = 1;                                               // ratio of canvas to window
        overflowSize = 0;                                                       // size of non-grid area
        overflowRatio = 2.5;                                                    // maximum ratio of length <-> width
        canvasMovesWithScroll = false;                                          // if canvas moves with the screen, this should be true
        block_count = 10;                                                       // number of cells per row and col
        dimensions = 500;                                                       // height and width of canvas in px
        ORIGINAL_GRID_PADDING = 20;                                             // normal padding for normal mode
        UNORIGINAL_GRID_PADDING = 20;                                           // padding for zoomed-out mode
        grid_padding = 20;                                                      // padding on single side of grid
        grid_size = (dimensions - grid_padding * 2) / block_count;
        block_size = .4 * grid_size;
        peg_size = .2 * grid_size;
        block_padding = .6 * grid_size;
        pointDirection = 'right';                                               // default pointing direction
        colorScheme = [                                                         // colorScheme: the most descriptive and easily understood variable name
            '#FF0000',                                                              // 'Red'
            '#FFD700',                                                              // 'Gold'
            '#0000FF',                                                              // 'Blue'
            '#FF7F50',                                                              // 'Coral'
            '#228B22',                                                              // 'ForestGreen'
            '#9400D3'];                                                             // 'DarkViolet'
        
        // water colors: 0d0d40, paleturquoise, royalblue
        // sub colors: 66 66 99, 
        
        // explosion variables
        explosionRadius = grid_size;                                            // how close ships have to be to take damage
        shipHP = 1;
        
        // particle variables
        fractures = 10;                                                         // each block produces (fractures^2) particles
        particleFramesOfExistence = 500;                                        // how long particles live, in frames
        forceScaling = 5;                                                       // force is divided by forceScaling then deconstructed into vx and vy
        forceRandomizationFactor = .25;                                         // .2 means 20% of the force is randomized
        dragForce = 1.02;                                                       // velocity is divided by this every frame
        dragForceLifespanScalingDivider = 10;                                   // determines how many frames per exponential increase in dragforce
        
        // transition variables
        currentlyTransitioning = false;                                         // turn this on and the transition starts, turns off when done
        framesPerBlock = 3;                                                     // how much of a block one frame transitions over
        foldIndentRatio = .2;                                                   // how trapezoid-y the fold is
            
        // menu variables
        bulletLength = dimensions / 3;
        bulletWidth = bulletLength / 10;
        
        ball = new Ball();
        transition = new Transition();
        menu = new Menu();
        crosshair = new Crosshair(20);
        
        loopInterval = setInterval(loop, 1000 / FPS);
        
        isMobile = /Mobi/.test(navigator.userAgent);                            // detects if mobile (ignore warning lol)
        
        sideShift = 0;
        
        window.addEventListener("resize", refresh);
        refresh();
    }
    initialize();
    
    
    
    
	// if mobile
	if (isMobile) {
        
        fractures = 3;
        
        crosshair.radius = 100;
        
        
        // fired when user puts finger on screen
        canvas.addEventListener("touchstart", touchHandler);
        
        // after touching, how the finger is moved
        canvas.addEventListener("touchmove", touchHandler);
        
        // when the user stops touching the screen
        canvas.addEventListener("touchend", touchEndHandler);
        
        //when a touch is canceled e.g. sliding off the game
        canvas.addEventListener("touchcancel", null);
        
        
        
        
    }
    else {
        
        
        /** todo: turn these into event listeners **/
        
        
        // mouse move
        window.onmousemove = function(event){ onMouseMoveHandler(event); };
        
        // left click
        window.onclick = function(event) { onClickHandler(event); };
        
        //window.onmousedown = function(event) { onMouseDownHandler(event); };
        //window.onmouseup = function(event) { onMouseUpHandler(event); };
        
        // right click
        window.oncontextmenu = function (event) { onRightClickHandler(event); };
        
        // key press
        window.onkeyup = function(event) { onKeyHandler(event); };
        window.onkeydown = function(event) { if (event.keyCode == 32 && event.target == document.body) { event.preventDefault(); } };
        window.onbeforeunload = function(event) { communicator.socket.close() };
	}
    
    
    
    
    
    
    
    
    function abortGame(text) {
        
        setBackground("rgba(0, 0, 0, .5");
        elements.removeAllElements();
        chat.deactivate();
        
        drawText(
            (dimensions + (isLandscape ? overflowSize : 0)) / 2, 
            (dimensions + (isLandscape ? 0 : overflowSize)) / 3,
            text,
            64,
            "verdana", 
            "white"
        );
        
        canvas.style.cursor = 'default';
        clearInterval(loopInterval);
        
        elements.addElement(
            "button",
            1 / 2 ,
            2 / 3, 
            200, 
            20, 
            'restart', 
            'restartButton'
            );
                                
    }
    
    var imageStack = new ImageStack();
    imageStack.addImageToLibrary("explosion 1", '/web/images/canvas/ansimuz/explosion-1.png', 'auto', 1);
    imageStack.addImageToLibrary("explosion 4", '/web/images/canvas/ansimuz/explosion-4.png', 'auto', 1);
    imageStack.addImageToLibrary("explosion 5", '/web/images/canvas/ansimuz/explosion-5.png', 'auto', 1);
    imageStack.addImageToLibrary("explosion 6", '/web/images/canvas/ansimuz/explosion-6.png', 'auto', 1);
    imageStack.addImageToLibrary("excellent ship", '/web/images/canvas/super excellento hand made artisan craft pixel-packed art/wonderfulship.png', 1, 1);
    imageStack.addImageToLibrary("terrified ship", '/web/images/canvas/super excellento hand made artisan craft pixel-packed art/badship.png', 1, 1);
    imageStack.addImageToLibrary("testo", "/web/images/canvas/japes/testo.png", 8, 4);
    
    
    var dingSounds = new AudioStack(.2);
    dingSounds.addAudioToLibrary("1", "/web/audio/canvas/anton/e0-ff.wav");
    dingSounds.addAudioToLibrary("2", "/web/audio/canvas/anton/e0-mf.wav");
    dingSounds.addAudioToLibrary("3", "/web/audio/canvas/anton/e0-pp.wav");
    dingSounds.addAudioToLibrary("4", "/web/audio/canvas/anton/f-ff.wav");
    dingSounds.addAudioToLibrary("5", "/web/audio/canvas/anton/f-mf.wav");
    dingSounds.addAudioToLibrary("6", "/web/audio/canvas/anton/f-pp.wav");
    dingSounds.addAudioToLibrary("7", "/web/audio/canvas/anton/g-ff.wav");
    dingSounds.addAudioToLibrary("8", "/web/audio/canvas/anton/g-mf.wav");
    dingSounds.addAudioToLibrary("9", "/web/audio/canvas/anton/g-pp.wav");
    dingSounds.addAudioToLibrary("10", "/web/audio/canvas/anton/a-ff.wav");
    dingSounds.addAudioToLibrary("11", "/web/audio/canvas/anton/a-mf.wav");
    dingSounds.addAudioToLibrary("12", "/web/audio/canvas/anton/a-pp.wav");
    dingSounds.addAudioToLibrary("13", "/web/audio/canvas/anton/b-ff.wav");
    dingSounds.addAudioToLibrary("14", "/web/audio/canvas/anton/b-mf.wav");
    dingSounds.addAudioToLibrary("15", "/web/audio/canvas/anton/b-pp.wav");
    dingSounds.addAudioToLibrary("16", "/web/audio/canvas/anton/c-ff.wav");
    dingSounds.addAudioToLibrary("17", "/web/audio/canvas/anton/c-mf.wav");
    dingSounds.addAudioToLibrary("18", "/web/audio/canvas/anton/c-pp.wav");
    dingSounds.addAudioToLibrary("19", "/web/audio/canvas/anton/d-ff.wav");
    dingSounds.addAudioToLibrary("20", "/web/audio/canvas/anton/d-mf.wav");
    dingSounds.addAudioToLibrary("21", "/web/audio/canvas/anton/d-pp.wav");
    dingSounds.addAudioToLibrary("22", "/web/audio/canvas/anton/e1-ff.wav");
    dingSounds.addAudioToLibrary("23", "/web/audio/canvas/anton/e1-mf.wav");
    dingSounds.addAudioToLibrary("24", "/web/audio/canvas/anton/e1-pp.wav");
    
    var shotSounds = new AudioStack(.1);
    shotSounds.addAudioToLibrary("tuck", "/web/audio/canvas/pjcohen/46564__pjcohen__ludwig-black-beauty-snare-drum-closed-rimshot-unprocessed.wav");
    shotSounds.addAudioToLibrary("tang", "/web/audio/canvas/pjcohen/46565__pjcohen__ludwig-black-beauty-snare-drum-open-rimshot-unprocessed.wav");
    shotSounds.addAudioToLibrary("tugg", "/web/audio/canvas/pjcohen/46566__pjcohen__ludwig-black-beauty-snare-drum-unprocessed.wav");
    
    var pingSound = new AudioStack(.1);
    pingSound.addAudioToLibrary("ping", "/web/audio/canvas/28693__infobandit__sonar.wav");
    
    var hitSounds = new AudioStack(.1);
    hitSounds.addAudioToLibrary("HRuuhB", "/web/audio/canvas/plagasul/71__plagasul__hruuhb.wav");
    hitSounds.addAudioToLibrary("HouU", "/web/audio/canvas/plagasul/73__plagasul__houu.wav");
    hitSounds.addAudioToLibrary("EH", "/web/audio/canvas/plagasul/70__plagasul__eh.wav");
    hitSounds.addAudioToLibrary("JoooAah", "/web/audio/canvas/plagasul/76__plagasul__joooaah.wav");
    hitSounds.addAudioToLibrary("oA-H", "/web/audio/canvas/plagasul/86__plagasul__oa-h.wav");
    
    var submergeSounds = new AudioStack(.1);
    submergeSounds.addAudioToLibrary("woosh", "/web/audio/canvas/25073__freqman__whoosh04.wav");
    submergeSounds.addAudioToLibrary("wave", "/web/audio/canvas/61011__kayyy__wave1.wav");
    submergeSounds.addAudioToLibrary("hydrophone", "/web/audio/canvas/267245__klankbeeld__wave-hydrophone-001.wav");
    
    var missSounds = new AudioStack(.1);
    missSounds.addAudioToLibrary("splash", "/web/audio/canvas/splash.wav");
    
    
    this.pause = new function(ms) { 
        clearInterval(loopInterval);
        new Promise(resolve => setTimeout(resolve, ms)).then(() => { 
            loopInterval = setInterval(loop, 1000 / FPS);
        }); 
    };
    
    
    function Communications() {
        
        this.socket = new WebSocket("wss://QuestionableBattleship.com:4680");
        this.socket.isConnected = false;
        
        this.reconnect = function() {
            this.socket.close();
            communicator = new Communications();
        };
        
        this.socket.onopen = function() {
            console.log("WebSocket Connected");
            this.isConnected = true;
        };
        
        this.socket.onclose = function() {
            console.log("WebSocket Disconnected");
            this.isConnected = false;
            abortGame("WebSocket Disconnected");
        };
        
        this.socket.onerror = function() {
            console.log('WebSocket Error');
        };
        this.socket.onmessage = function(event) {
            console.log("Recieved: " + event.data);
            var object = JSON.parse(event.data);
            // proceed to actually use the data
            
            switch(object.type) {
                case "join_game": 
                    if (object.status) {
                        player_id = object.player_id;
                        players.addPerson(player_name, player_id);
                        
                        game_id = object.game_id;
                        for (var i = 0; i < object.opponents.length; i++) {
                            players.addPerson(object.opponents[i].name, object.opponents[i].id);
                            opponent_id = object.opponents[i].id;
                        }
                        for (var i = 0; i < object.ships.length; i++) {
                            players.addStartingShip(player_id, object.ships[i].ship_name, object.ships[i].ship_id, object.ships[i].ship_length);
                        }
                        players.queueStartingShips(player_id);
                    } else console.error("Failed to join room because " + object.reason);
                    break;
                    
                    
                case "start_game": // both parties agreed to start the game
                    console.log("start_game sent successfully. ");
                    break;
                    
                    
                case "place_ships":
                    if (object.status) {
                        var results = object.results;
                        for (var i = 0; i < results.length; i++) {
                            var ship_id = results[i].ship_id;
                            if (!results[i].status) {
                                console.error("Ship (" + ship_id + "was unable to be placed because " + results[i].reason);
                            }
                        }
                    } else console.error("Failed to place any ships because " + object.reason);
                    break;
                    
                    
                case "make_shot": 
                    if (object.status) {
                        switch(object.result) {
                            case "sink":
                                console.log("ship sunk");
                            case "hit":
                                players.queueShip(opponent_id, "hit_ship", "temp_id", 1);
                                players.addShip(opponent_id, object.location, "up", "#696969", false);
                                break;
                            case "miss":
                                players.queueShip(opponent_id, "miss_ship", "temp_id", 1);
                                players.addShip(opponent_id, object.location, "up", "#00FFFF", false);
                                break;
                            default: console.log("object.result not recognized.");
                        }
                    } else console.error("Failed to make shot because " + object.reason);
                    break;
                    
                    
                case "update":
                    switch(object.update_type) {
                        case "start_game":
                            if (object.status) {
                                turn = object.first_turn == player_id;
                                if (object.first_turn == player_id) {
                                    console.log("it's your turn!");
                                }
                                if (object.first_turn == opponent_id) {
                                    console.log("it's the other guy's turn!");
                                }
                                
                                nextState();
                                elements.removeAllElements();
                                
                            } else console.error("Failed to start game because " + object.reason);
                            break;
                        case "start_attack":
                            nextState();
                            break;
                        case "receive_shot":
                            if (object.coordinates) {
                                var locationVerification = XY2RC(object.coordinates);
                                if (object.location.row != locationVerification.row || object.location.col != locationVerification.col) {
                                    console.error("Coordinates and Location of shot received do not match!");
                                    console.log("discarding coordinates");
                                    object.coordinates = false;
                                }
                            }
                            if (!object.coordinates) {
                                object.coordinates = RC2XY(object.location, "center");
                                object.coordinates.x /= dimensions;
                                object.coordinates.y /= dimensions;
                            }
                            
                            // if the opponent missed
                            if (!players.isThereABlockHere(player_id, object.location)) {
                                players.queueShip(player_id, "miss_ship", "temp_id", 1);
                                players.addShip(player_id, object.location, "up", "#00FFFF", false);
                            }
                            
                            players.queueShot(player_id, object.coordinates.x, object.coordinates.y);
                            players.addPeg(player_id, object.location);
                            break;
                        case "opponent_joined":
                            opponent_id = object.opponent.id;
                            players.addPerson(object.opponent.name, object.opponent.id);
                            for (var i = 0; i < object.opponent.ships.length; i++) {
                                players.addStartingShip(
                                    object.opponent.id, 
                                    object.opponent.ships[i].ship_name, 
                                    object.opponent.ships[i].ship_id, 
                                    object.opponent.ships[i].ship_length);
                            }
                            
                            
                            break;
                        case "abort_game":
                            console.log(object.reason);
                            abortGame("Opponent Left The Game");
                            break;
                        case "game_end":
                            console.log("The Winner has been decided. ");
                            winner_id = object.winner;
                            break;
                        default: console.error("Unrecognized Update Type: " + object.update_type);
                    }
                    break;
                
                
                default: console.error("Unrecognized message from server");
            }
        };
        
        this.send = function(data) {
            console.log("Sent: " + JSON.stringify(data));
            this.socket.send(JSON.stringify(data));
        };
        
        this.sendData = function(type) {
            switch(type) {
                case "join_game":
                    this.send({
                        "type" : "join_game",
                        "player_name" : player_name, 
                        "game_name" : game_name
                    });
                break;
                case "start_game":
                    this.send({
                        "type" : "start_game",
                        "game_id" : game_id,
                        "player_id" : player_id,
                    });
                break;
                case "place_ships":
                    this.send(players.getShipPacket(player_id));
                break;
                case "make_shot":
                    for (var i = 0; i < players.shotsIncoming(opponent_id); i++) {
                        var shot = players.shotDetail(opponent_id, i);
                        var location = XY2RC(shot);
                        var shotPacket = {
                            "type" : "make_shot",
                            "game_id" : game_id,
                            "shooter_id" : player_id,
                            "shootee_id" : opponent_id,
                            "location" : location,
                            "coordinates" : shot
                        };
                        this.send(shotPacket);
                    }
                break;
                default: console.error("type in Connections.sendData unrecognized. ");
            }
        };
    }
    
        
	context.translate(.5, .5);
	
    
    
    
    function nextState() {
        
        console.log("changed states");
        
        players.reloadAll();
        
        switch(currentGameState) {
            case 'initialization':
                currentGameState = 'connection';
                
                for (var i = 0; i < 100; i++) {
                    randomStuff.addRandomSpeck();
                }
                submergeSounds.playRandom(false);
                break;
            case 'connection':
                currentGameState = 'opponent';
                loadPercentage.opacity = 1;
                submergeSounds.playRandom(false);
                break;
                
            case 'opponent':
                currentGameState = 'setup';
                imageStack.clearActive();
                chat.activate();
                break;
            case 'setup':
                
                currentGameState = turn ? "attack" : "defend";
                break;
            case 'attack':
                
                
                if (winner_id) {
                    currentGameState = "endgame";
                    elements.addElement(
                        "button",
                        1 / 2 ,
                        2 / 3, 
                        200, 
                        20, 
                        'restart', 
                        'restartButton'
                        );
                } else {
                    currentGameState = 'defend';
                }
                
                break;
            case 'defend':
                if (winner_id) {
                    currentGameState = "endgame";
                    elements.addElement(
                        "button",
                        1 / 2 ,
                        2 / 3, 
                        200, 
                        20, 
                        'restart', 
                        'restartButton'
                        );
                }
                else {
                    currentGameState = 'attack';
                }
                
                break;
            case 'endgame':
                
                
                break;
                
            default: console.error("Game Flow went off the rails somehow");
        }
    }


    function Transition() {
        
        this.fill = true;                                                       // true: fill, false: empty
        this.covered = false;                                                   // whether the board is covered by the transition or not
        this.reversed = -1;                                                     // -1: under fold; 1: over fold
        this.currentDirection = Math.floor(4 * Math.random());
        
        
        this.covers = [];
        
        // sets up the 3d array, where directionChoice determines the diagonal on which the transition unfolds
        this.randomizeDirections = function(directionChoice, type) {
            
            this.covers = [];
                var directions;
                var diagonal;
                switch(directionChoice) {
                    case 0:
                        directions = ['right', 'down'];
                        diagonal = 'southeast';
                        break;
                    case 1: 
                        directions = ['down', 'left'];
                        diagonal = 'southwest';
                        break;
                    case 2: 
                        directions = ['left', 'up'];
                        diagonal = 'northwest';
                        break;
                    case 3: 
                        directions = ['up', 'right'];
                        diagonal = 'northeast';
                        break;
                    default: console.error("Something went wrong with directionChoice in randomizeDirections()");
                }  
                
                for (var i = 0; i < block_count; i++) {
                    this.covers.push([]);
                    for (var ii = 0; ii < block_count; ii++) {
                        var frame = this.covered ? framesPerBlock : -framesPerBlock;
                        
                        //normally is random
                        var direction = directions[Math.floor(Math.random() * directions.length)];
                        
                        //special cases
                        
                        switch(type) {
                            case 'extend':
                                // if first row
                                if (i == 0) {
                                    switch(diagonal) {
                                        case 'southeast':
                                            direction = 'right';
                                            break;
                                        case 'southwest':
                                            direction = 'left';
                                            break;
                                    }
                                }
                                // if first col
                                if (ii == 0) {
                                    switch(diagonal) {
                                        case 'southeast':
                                            direction = 'down';
                                            break;
                                        case 'northeast':
                                            direction = 'up';
                                            break;
                                    }
                                }
                                // if last row
                                if (i == block_count - 1) {
                                    switch(diagonal) {
                                        case 'northwest':
                                            direction = 'left';
                                            break;
                                        case 'northeast':
                                            direction = 'right';
                                            break;
                                    } 
                                }
                                // if last col
                                if (ii == block_count - 1) {
                                    switch(diagonal) {
                                        case 'northwest':
                                            direction = 'up';
                                            break;
                                        case 'southwest':
                                            direction = 'down';
                                            break;
                                    }
                                }
                                break;
                            case 'retract':
                                // if first row
                                if (i == 0) {
                                    switch(diagonal) {
                                        case 'northwest':
                                            direction = 'left';
                                            break;
                                        case 'northeast':
                                            direction = 'right';
                                            break;
                                    }
                                }
                                // if first col
                                if (ii == 0) {
                                    switch(diagonal) {
                                        case 'northwest':
                                            direction = 'up';
                                            break;
                                        case 'southwest':
                                            direction = 'down';
                                            break;
                                    }
                                }
                                // if last row
                                if (i == block_count - 1) {
                                    switch(diagonal) {
                                        case 'southeast':
                                            direction = 'right';
                                            break;
                                        case 'southwest':
                                            direction = 'left';
                                            break;
                                    } 
                                }
                                // if last col
                                if (ii == block_count - 1) {
                                    switch(diagonal) {
                                        case 'southeast':
                                            direction = 'down';
                                            break;
                                        case 'northeast':
                                            direction = 'up';
                                            break;
                                    }
                                }
                                break;
                            default: console.error("Type in randomizeDirections() is incorrect");
                        }
                        
                        this.covers[i].push([frame, direction]);
                    }
                }
        };
        
        
        // actually sets up the array
        this.randomizeDirections(this.currentDirection, 'extend');
        
        
        // called each frame
        this.update = function() {
            
            if (!transitionToggle && currentlyTransitioning) {
                currentlyTransitioning = false;
                nextState();
                return false;
            }
            
            // if no action is required
            if (!currentlyTransitioning && this.covered == false) return;
            
            
            this.covered = this.fill ? currentlyTransitioning : !currentlyTransitioning;
            
            // i: row, ii: col
            for (var i = 0; i < this.covers.length; i++) {
                for (var ii = 0; ii < this.covers[i].length; ii++) {
                    
                    // folds out the current transition block
                    // checks: is currently transitioning, is not finished transitioning, and originating block is completely unfolded or is an edge
                    if (currentlyTransitioning) {
                        if (this.fill && this.covers[i][ii][0] < framesPerBlock) {
                            this.covered = false;
                            if (
                                    this.covers[i][ii][1] == 'down'     && (i == 0                 || this.covers[i - 1][ii][0] == framesPerBlock) ||
                                    this.covers[i][ii][1] == 'right'    && (ii == 0                || this.covers[i][ii - 1][0] == framesPerBlock) ||
                                    this.covers[i][ii][1] == 'up'       && (i == block_count - 1   || this.covers[i + 1][ii][0] == framesPerBlock) ||
                                    this.covers[i][ii][1] == 'left'     && (ii == block_count - 1  || this.covers[i][ii + 1][0] == framesPerBlock)
                                ) {
                                    this.covers[i][ii][0]++;
                            }
                        }
                        
                        if (!this.fill && this.covers[i][ii][0] > -framesPerBlock) {
                            this.covered = true;
                            if (
                                    this.covers[i][ii][1] == 'down'     && (i == block_count - 1   || this.covers[i + 1][ii][0] == -framesPerBlock) ||
                                    this.covers[i][ii][1] == 'right'    && (ii == block_count - 1  || this.covers[i][ii + 1][0] == -framesPerBlock) ||
                                    this.covers[i][ii][1] == 'up'       && (i == 0                 || this.covers[i - 1][ii][0] == -framesPerBlock) ||
                                    this.covers[i][ii][1] == 'left'     && (ii == 0                || this.covers[i][ii - 1][0] == -framesPerBlock)
                                ) {
                                    this.covers[i][ii][0]--;
                            }
                        }
                    }
                    
                    // renders this transition block
                    this.transitionRender(RC2XY([i, ii], 'grid'), this.covers[i][ii]);
                }
            }
            
            // if board is covered, stop transitioning and setup for emptying the board, also vice versa
            if (this.covered != !this.fill) {
                currentlyTransitioning = false;
                this.fill = !this.fill;
                this.currentDirection = this.fill ? Math.floor(4 * Math.random()) : this.currentDirection < 2 ? this.currentDirection + 2 : this.currentDirection - 2;
                this.randomizeDirections(this.currentDirection, this.fill ? 'extend' : 'retract');
                
                if (!this.fill) {
                    nextState();
                    var textualOffset = 2;
                    var placement = dimensions * 4 / 10;
                    context.font = '64px verdana';
                    context.fillStyle = 'yellow';
                    context.fillText(currentGameState, placement - textualOffset, placement - textualOffset);
                    context.fillStyle = 'blue';
                    context.fillText(currentGameState, placement, placement);
                    context.fillStyle = 'red';
                    context.fillText(currentGameState, placement + textualOffset, placement + textualOffset);
                    
                    // clear particles
                    
                    //pause(2000);
                    endTurn();
                }
            }
        };
        
        
        
        this.transitionRender = function(coordinates, cover) {
            var frame = cover[0];
            var direction = cover[1];
            
            var percentage = frame / framesPerBlock;
            
            var foldIndent = (grid_size * foldIndentRatio * (Math.abs(frame / framesPerBlock) - 1)) * this.reversed;
            
            
            if (frame > 0) {

                context.beginPath();
                context.strokeStyle = 'Black';
                context.fillStyle = 'Gainsboro';
                context.lineWidth = 1;
                
                switch(direction) {
                    case 'right': // southwest -> northwest -> northeast -> southeast -> southwest
                        context.moveTo(coordinates[0], coordinates[1] + grid_size);
                        context.lineTo(coordinates[0], coordinates[1]);
                        context.lineTo(coordinates[0] + (grid_size * percentage), coordinates[1] + foldIndent);
                        context.lineTo(coordinates[0] + (grid_size * percentage), coordinates[1] + grid_size - foldIndent);
                        context.lineTo(coordinates[0], coordinates[1] + grid_size);
                        break;
                    case 'down': // northwest -> northeast -> southeast -> southwest -> northwest
                        context.moveTo(coordinates[0], coordinates[1]);
                        context.lineTo(coordinates[0] + grid_size, coordinates[1]);
                        context.lineTo(coordinates[0] + grid_size - foldIndent, coordinates[1] + (grid_size * percentage));
                        context.lineTo(coordinates[0] + foldIndent, coordinates[1] + (grid_size * percentage));
                        context.lineTo(coordinates[0], coordinates[1]);
                        break;
                    case  'left': // northeast -> southeast -> southwest -> northwest -> northeast
                        context.moveTo(coordinates[0] + grid_size, coordinates[1]);
                        context.lineTo(coordinates[0] + grid_size, coordinates[1] + grid_size);
                        context.lineTo(coordinates[0] + (grid_size * (1 - percentage)), coordinates[1] + grid_size - foldIndent);
                        context.lineTo(coordinates[0] + (grid_size * (1 - percentage)), coordinates[1] + foldIndent);
                        context.lineTo(coordinates[0] + grid_size, coordinates[1]);
                        break;
                    case 'up': // southeast -> southwest -> northwest -> northeast -> southeast
                        context.moveTo(coordinates[0] + grid_size, coordinates[1] + grid_size);
                        context.lineTo(coordinates[0], coordinates[1] + grid_size);
                        context.lineTo(coordinates[0] + foldIndent, coordinates[1] + (grid_size * (1 - percentage)));
                        context.lineTo(coordinates[0] + grid_size - foldIndent, coordinates[1] + (grid_size * (1 - percentage)));
                        context.lineTo(coordinates[0] + grid_size, coordinates[1] + grid_size);
                        break;
                    default: console.error("direction in transitionRender() is " + direction);
                }
                context.fill(); 
                context.stroke();
                context.closePath();
            }
        };
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////// MENU
    
    function Menu() {
        this.menuMode = 'setup';
        this.nw = {"x" : 0, "y" : 0};
        this.width = 0;
        this.height = 0;
        this.update = function() {
            if (isLandscape) {
                this.nw.x = dimensions;
                this.nw.y = 0;
                this.width = overflowSize;
                this.height = dimensions;
            } else {
                this.nw.x = 0;
                this.nw.y = dimensions;
                this.width = dimensions;
                this.height = overflowSize;
            }
            
	        context.beginPath();
		    context.fillStyle = 'Gainsboro';
		    context.fillRect(
		        this.nw.x, 
		        this.nw.y, 
		        this.width, 
		        this.height);
		        
		        
		        
            var x = this.nw.x + block_padding;
            var y = this.nw.y + (block_padding * 1.5);
            
            
            switch(currentGameState) {
                case 'setup':
                    for (var i = 0; i < players.queueCount(player_id); i++) {
                        drawShipIcon(
                            x, 
                            y + (block_padding + block_size) * i, 
                            block_size, 
                            block_padding / 4, 
                            players.getQueuedShipLength(player_id, i), 
                            "horizontal");
                    }
                    break;
                case 'attack':
                    for (var i = 0; i < players.shotsRemaining(opponent_id); i++) {
                        drawBullet(
                            x,
                            y + (block_padding + block_size) * i,
                            bulletLength, 
                            bulletWidth, 
                            'horizontal');
                    }
                    break;
            }
        };
    }
    
    // length = long side of bullet
    function drawBullet(tipX, tipY, length, width, direction) {
        
        context.beginPath();
        context.fillStyle = 'darkgrey';
        
        switch(direction) {
            case 'horizontal':
                context.moveTo(
                    tipX, 
                    tipY
                );
                context.quadraticCurveTo(
                    tipX + (length * 1/10),
                    tipY + width,
                    tipX + (length * 3/10), 
                    tipY + width
                );
                context.lineTo(
                    tipX + (length * 8/10),
                    tipY + width
                );
                context.lineTo(
                    tipX + (length * 8/10),
                    tipY - width
                );
                context.lineTo(
                    tipX + (length * 3/10),
                    tipY - width
                );
                context.quadraticCurveTo(
                    tipX + (length * 1/10),
                    tipY - width,
                    tipX,
                    tipY
                );
                break;
            case 'vertical':
                context.moveTo(
                    tipX, 
                    tipY
                );
                context.quadraticCurveTo(
                    tipX - width,
                    tipY + (length * 1/10),
                    tipX - width, 
                    tipY + (length * 3/10)
                );
                context.lineTo(
                    tipX - width,
                    tipY + (length * 8/10)
                );
                context.lineTo(
                    tipX + width,
                    tipY + (length * 8/10)
                );
                context.lineTo(
                    tipX + width,
                    tipY + (length * 3/10)
                );
                context.quadraticCurveTo(
                    tipX + width,
                    tipY + (length * 1/10),
                    tipX,
                    tipY
                );
                break;
            default: console.error("bullet has no direction");
        }
        context.fill();
        context.closePath();
    }
    
    function drawShipIcon(tipX, tipY, size, padding, number, direction) {
        for (var i = 0; i < number; i++) {
            context.beginPath();
            context.fillStyle = 'darkgrey';
            context.fillRect(
                direction == 'horizontal' ? tipX + (size + padding) * i : tipX, 
                direction == 'vertical' ? tipY + (size + padding) * i : tipY, 
                size, 
                size);
        }
        
    }
    
    
    
 
    //////////////////////////////////////////////////////////////////////////////////////////////////// EXPLOSIONS (DIRECT FORCE APPLICATIONS)
    function Crosshair(radius) {
        this.x = mouseX;
        this.y = mouseY;
        this.radius = radius;
        this.color = 'Black';
        this.crosshairSideLength = .5;
        
        
        this.update = function() {
            this.x = mouseX;
            this.y = mouseY;
            
            switch(currentGameState) {
                case 'setup':
                    canvas.style.cursor = 'default';
                    if (players.queueCount(player_id)) { 
                        multipleSelectionAction(
                            XY2RC({"x" : mouseX / dimensions, "y" : mouseY / dimensions}), 
                            pointDirection, 
                            players.getQueuedShipLength(player_id, 0), 
                            'highlight'
                        );
                    }
                    break;
                case 'attack':
                    canvas.style.cursor = 'none';
                    if (players.shotsRemaining(opponent_id)) { 
                        multipleSelectionAction(
                            XY2RC({"x" : mouseX / dimensions, "y" : mouseY / dimensions}), 
                            pointDirection, 
                            1, 
                            'targetingHighlight'
                        ); 
                    }
                    
                    // circle
                    context.beginPath();
                    context.strokeStyle = this.color;
                    context.lineWidth = 1;
                    context.arc(this.x, this.y, this.radius, 0, Math.PI*2, true);
                    context.stroke();
                    
                    context.beginPath();
                    context.arc(this.x, this.y, 1, 0, Math.PI*2, true);
                    
                    // lines
                    
                    context.moveTo(this.x, this.y - this.radius * (1 + this.crosshairSideLength));
                    context.lineTo(this.x, this.y - this.radius * (1 - this.crosshairSideLength));
                    
                    context.moveTo(this.x, this.y + this.radius * (1 - this.crosshairSideLength));
                    context.lineTo(this.x, this.y + this.radius * (1 + this.crosshairSideLength));
                    
                    context.moveTo(this.x - this.radius * (1 + this.crosshairSideLength), this.y);
                    context.lineTo(this.x - this.radius * (1 - this.crosshairSideLength), this.y);
                    
                    context.moveTo(this.x + this.radius * (1 + this.crosshairSideLength), this.y);
                    context.lineTo(this.x + this.radius * (1 - this.crosshairSideLength), this.y);
                    
                    
                    context.stroke();
                    context.closePath();
                    
            		break;
            	default: 
            	canvas.style.cursor = 'default';
            }
        };
    }
    
    // radius - distance this
    function modifiedMegumin(x, y, shipsArray) {
        var location = XY2RC([x, y]);
        var row = location[0];
        var col = location[1];
        
        for (var i = 0; i < shipsArray.length; i++) {
            for (var ii = 0; ii < shipsArray[i].shipBlocks.length; ii++) {
                if (shipsArray[i].shipBlocks[ii].row == row && shipsArray[i].shipBlocks[ii].col == col) {
                    shipsArray[i].impact(ii, explosionRadius, [x, y]);
                }
            }
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////// CONVERSIONS (ROWS/COLUMNS <-> X/Y COORDINATES)
    
    // x and y coordinates to row and col
    function XY2RC(coordinates) {
        
        // gets the row and col from x and y values
        var col = Math.floor((coordinates.x * dimensions - grid_padding) / grid_size);
    	var row = Math.floor((coordinates.y * dimensions - grid_padding) / grid_size);
    	
    	// input validation: if the x,y points outside of the area
    	if (col < 0 || row < 0 || col >= block_count || row >= block_count) { return false; }
    	
        return {"row" : row, "col" : col};
    }
    
    // row and col to x and y coordinates
    function RC2XY(location, type) {
        
        // gets the top left point of the row/col combination
        var x = grid_padding + (location.col * grid_size);
        var y = grid_padding + (location.row * grid_size);
        
        // if type is a selection, push in a bit, if block, push in more
        switch (type) {
            case "grid":
                break;
            case "peg":
                x += (grid_size / 2) - (peg_size / 2);
                y += (grid_size / 2) - (peg_size / 2);
                break;
            case "block":
                x += (grid_size / 2) - (block_size / 2);
                y += (grid_size / 2) - (block_size / 2);
                break;
            case "selection":
                x += block_padding / 4;
                y += block_padding / 4;
                break;
            case "center":
                x += grid_size / 2;
                y += grid_size / 2;
                break;
        }
        
        return {"x" : x, "y" : y};
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////// APPLYING ACTIONS TO MULTIPLE BLOCKS
    
    function multipleSelectionAction(location, direction, length, action) {
        
        if (!location) return false;
        
        var row = location.row;
        var col = location.col;
        
        var selections = [];
        
        for (var i = 0; i < length; i++) {
            switch(direction) {
                case 'left':
                    selections.push({
                        "row" : row,
                        "col" : col - i
                    });
                    break;
                case 'right':
                    selections.push({
                        "row" : row, 
                        "col" : col + i
                    });
                    break;
                case 'up':
                    selections.push({
                        "row" : row - i,
                        "col" : col
                    });
                    break;
                case 'down':
                    selections.push({
                        "row" : row + i, 
                        "col" : col
                    });
                    break;
                default: console.error("multipleSelectionAction has no direction");
            }
        }
        
        // more input validation
        var validSelections = true;
        
        // detect out-of-bounds selections: cut and show input is invalid
        for (var i = selections.length - 1; i >= 0; i--) {
            // row and cols for out-of-bounds
            if (    selections[i].row < 0 
                ||  selections[i].row > block_count - 1
                ||  selections[i].col < 0 
                ||  selections[i].col > block_count - 1
                ) {
                validSelections = false;
                selections.splice(i, 1);
            }
        }
        
        
        // detecting whether there is a conflict of interest
        if (currentGameState == "setup") {
            for (var i = 0; i < selections.length; i++) {
                if (players.isThereABlockHere(player_id, selections[i])) {
                    validSelections = false;
                }
            }
        }
        
        switch(action) {
            case "highlight":
                // if valid selection, highlight; if not valid, highlight in red
                for (var i = 0; i < selections.length; i++) {
                    highlightSelection(selections[i], validSelections ? 'LightGrey' : '#FFCCCC');
                }
                break;
            case "targetingHighlight":
                if (!players.isThereAPegHere(opponent_id, location)) {
                    highlightSelection(selections[0], 'LightGrey');
                }
                break;
            case "add":
                //var color = '#' + Math.floor(Math.random()*16777215).toString(16);        // random color
                var color = colorScheme[Math.floor(colorScheme.length * Math.random())];
                // if valid, add; if invalid, don't
                if (validSelections) {
                    players.addShip(player_id, location, direction, color, true);
                }
                break;
                
            default: console.error("multipleSelectionAction has no action");
        }
    }
    
    
    // highlighting the currently selected block
    function highlightSelection(location, color) {
        if (!location) {
            return false;
        }
        
        var coordinates = RC2XY(location, 'selection');
        
        context.fillStyle = color;
        context.fillRect(
            coordinates.x, 
            coordinates.y, 
            grid_size - block_padding / 2, 
            grid_size - block_padding / 2
        );
    }
    
    
    
    
    function GamePlayers() {
        var people = [];
        this.salvoSize = 1;
        
        this.count = function() {
            return people.length;
        };
        
        this.update = function(user_id) {
            people[getIndex(user_id)].board.update();
        };
        
        //////////////////////////////////////////////////////////////////////////////////////////////////// TRACKERS
        
        this.addPeg = function(user_id, location) {
            people[getIndex(user_id)].board.tracker.addShot(location);
        };
        
        this.changePegColor = function(user_id, location, color) {
            people[getIndex(user_id)].board.tracker.changeColor(location, color);
        };
        
        this.isThereAPegHere = function(user_id, location) {
            return people[getIndex(user_id)].board.tracker.isThereAPegHere(location);
        };
        
        //////////////////////////////////////////////////////////////////////////////////////////////////// PARTICLES
        
        this.addParticle = function(user_id, coordinates, force, forceSource, size, color) {
            people[getIndex(user_id)].board.particles.addParticle(coordinates, force, forceSource, size, color);
        };
        
        this.clearParticles = function(user_id) {
            people[getIndex(user_id)].board.particles.clear();
        };
        
        //////////////////////////////////////////////////////////////////////////////////////////////////// SHIPS
        
        this.addShip = function(user_id, location, direction, color, visibility) {
            people[getIndex(user_id)].board.ships.addShip(location, direction, color, visibility);
        };
        
        this.queueShip = function(user_id, name, identifier, length) {
            people[getIndex(user_id)].board.ships.queueShip(name, identifier, length);
        };
        
        this.addStartingShip = function(user_id, name, identifier, length) {
            people[getIndex(user_id)].board.ships.addStartingShip(name, identifier, length);
        };
        
        this.queueStartingShips = function(user_id) {
            people[getIndex(user_id)].board.ships.queueStartingShips();
        };
        
        this.getQueuedShipLength = function(user_id, index) {
            return people[getIndex(user_id)].board.ships.getQueuedShipLength(index);
        };
        
        this.isThereABlockHere = function(user_id, location) {
            return people[getIndex(user_id)].board.ships.isThereABlockHere(location);
        };
        
        this.queueCount = function(user_id) {
            return people[getIndex(user_id)].board.ships.queueCount();
        };
        
        this.shipCount = function(user_id) {
            return people[getIndex(user_id)].board.ships.count();
        };
        
        //////////////////////////////////////////////////////////////////////////////////////////////////// SHOTS
        
        this.queueShot = function(user_id, x, y) {
            people[getIndex(user_id)].board.queuedShots.queueShot(x, y);
        };
        
        this.fireShots = function(user_id) {
            people[getIndex(user_id)].board.queuedShots.fire();
        };
        
        this.shotsIncoming = function(user_id) {
            return people[getIndex(user_id)].board.queuedShots.count();
        };
        
        this.shotDetail = function(user_id, index) {
            return people[getIndex(user_id)].board.queuedShots.shotDetail(index);
        };
        
        this.setSalvoSize = function(number) {
            for (var i = 0; i < people.length; i++) {
                people[i].board.queuedShots.salvoSize = number;
            }
            this.salvoSize = number;
        };
        
        this.reloadAll = function() {
            for (var i = 0; i < people.length; i++) {
                people[i].board.queuedShots.reload();
            }
        };
        
        this.shotsRemaining = function(user_id) {
            return people[getIndex(user_id)].board.queuedShots.salvoSize - people[getIndex(user_id)].board.queuedShots.count();
        };
        
        //////////////////////////////////////////////////////////////////////////////////////////////////// PEOPLE
        
        
        function getIndex(user_id) {
            for (var i = 0; i < people.length; i++) {
                if (people[i].player_id == user_id) {
                    return i;
                }
            }
            console.error("Did not find player with id: " + user_id + ".");
        }
        
        this.addPerson = function(player_name, player_id) {
            imageStack.activateSpriteImage(!people.length ? "excellent ship" : "terrified ship", 2, 2, true, false, false, 1);
            people.push({
                "player_name" : player_name,
                "player_id" : player_id,
                "board" : new this.Board()
            });
            console.log("Person " + player_name + " with id " + player_id + " added. ");
        };
        
        this.subtractPerson = function(user_id) {
            people.splice(getIndex(user_id), 1);
        };
        
        //////////////////////////////////////////////////////////////////////////////////////////////////// 
        
        
        this.getShipPacket = function(user_id) {
            return people[getIndex(user_id)].board.ships.getShipPacket();
        };
        
        
        this.Board = function () {
            this.tracker = new Tracker();
            this.particles = new Particles();
            this.ships = new Ships();
            
            var attackInterval = 2000;
            var initialInterval = 1000;
            
            
            this.queuedShots = new QueuedShots();
            
            this.update = function() {
                drawGrid();
                
                this.tracker.update();
                this.particles.update();
                this.ships.update();
                
                if (this.queuedShots.ready) {
                    for (var i = 0; i < this.queuedShots.shots.length; i++) {
                        setTimeout(megumin.bind(this, this.queuedShots.shots[i]), attackInterval * i + initialInterval);
                    }
                    setTimeout(nextState, attackInterval * this.queuedShots.shots.length + initialInterval);
                    this.queuedShots.shots = [];
                    this.queuedShots.ready = false;
                }
            };
            
            
            function megumin(coordinates) {
                imageStack.activateSpriteImage("explosion 6", coordinates.x * dimensions, coordinates.y * dimensions, false, false, 3);
                
                var blockIndex = this.ships.isThereABlockHere(XY2RC(coordinates));
                
                if (blockIndex) {
                    this.ships.impact(blockIndex, explosionRadius, coordinates);
                }
            }
            
            
            function drawGrid() {
                
                // draw borders
                context.moveTo(0, 0);
                context.lineTo(dimensions, 0);
                context.lineTo(dimensions, dimensions);
                context.lineTo(0, dimensions);
                context.lineTo(0, 0);
                
                
                
                // draws vertical lines
                // starts at top after grid_padding, ends at bottom before grid_padding, + 1 is for rounding
                for (var x = grid_padding; x <= (dimensions - grid_padding + 1); x += grid_size) {
                    context.moveTo(x, grid_padding);
                    context.lineTo(x, dimensions - grid_padding);
                }
                
                // draws horizontal lines
                for (var x = grid_padding; x <= (dimensions - grid_padding + 1); x += grid_size) {
                    context.moveTo(grid_padding, x);
                    context.lineTo(dimensions - grid_padding, x);
                }
                
                context.lineWidth = 0.1;
                context.strokeStyle = 'black';
                context.stroke();
            }
            
            function QueuedShots() {
                this.shots = [];
                this.shotsTaken = 0;
                
                this.ready = false;
                this.salvoSize = 1;
                
                
                this.queueShot = function(x, y) {
                    this.shotsTaken++;
                    this.shots.push({
                        "x" : x,
                        "y" : y
                    });
                };
                
                this.reload = function() {
                    this.shotsTaken = 0;
                };
                
                this.count = function() {
                    return this.shotsTaken;
                };
                
                this.shotDetail = function(index) {
                    return this.shots[index];
                };
                
                this.isLoaded = function() {
                    return (this.shots.length >= players.salvoSize);
                };
                
                this.fire = function() {
                    this.ready = true;
                };
            }
            
            function Ships() {
                var ships = [];
                var shipQueue = [];
                var startingShips = [];
                
                
                this.impact = function(indices, force, forceSource) {
                    ships[indices.ship].impact(indices.block, force, forceSource);
                };
                
                this.getShipPacket = function() {
                    var shipPacket = {
                        "type" : "place_ships", 
                        "player_id" : player_id,
                        "game_id" : game_id,
                        "placements" : []
                    };
                    
                    for (var i = 0; i < ships.length; i++) {
                        var shipDetails = {
                            "ship_id" : ships[i].id,
                            "direction" : ships[i].direction,
                            "location" : ships[i].location
                        };
                        shipPacket.placements.push(shipDetails);
                    }
                    return shipPacket;
                };
                
                this.isThereABlockHere = function(location) {
                    for (var i = 0; i < ships.length; i++) {
                        for (var ii = 0; ii < ships[i].shipBlocks.length; ii++) {
                            var currentBlock = ships[i].shipBlocks[ii];
                            if (location.row == currentBlock.location.row && location.col == currentBlock.location.col) {
                                return {
                                    "ship" : i,
                                    "block" : ii
                                };
                            }
                        }
                    }
                    return false;
                };
                
                
                this.count = function() {
                    return ships.length;
                };
                
                this.queueCount = function() {
                    return shipQueue.length;
                };
                
                this.addStartingShip = function(name, identifier, length) {
                    startingShips.push({
                        "name" : name,
                        "identifier" : identifier, 
                        "length" : length
                    });
                    console.log("Starting Ship: " + JSON.stringify(startingShips[startingShips.length - 1]));
                };
                
                this.queueStartingShips = function() {
                    shipQueue = startingShips;
                    console.log("Starting Ships Queued");
                };
                
                this.queueShip = function(name, identifier, length) {
                    shipQueue.push({
                        "name" : name,
                        "identifier" : identifier,
                        "length" : length
                    });
                    console.log("Queued: " + JSON.stringify(shipQueue[shipQueue.length - 1]));
                };
                
                this.getQueuedShipLength = function(index) {
                    return shipQueue[index].length;
                };
                
                this.addShip = function(location, direction, color, visibility) {
                    ships.push(new Ship(shipQueue[0].name, shipQueue[0].identifier, location, shipQueue[0].length, direction, color, visibility));
                    shipQueue.splice(0, 1);
                };
                
                
                this.update = function() {
                    for (var i = ships.length - 1; i >= 0; i--) {
                        ships[i].updateShip();
                        if (ships[i].sunk) {
                            ships.splice(i, 1);
                        }
                    }
                };
                
                
                
                function Ship(name, identifier, location, length, direction, color, visibility) {
                    
                    console.log("Ship initialized: " + name + ", " + identifier + ", " + location.row + location.col + ", " + direction);
                    
                    this.name = name;
                    this.id = identifier;
                    this.length = length;
                    this.visibility = visibility;
                    this.sunk = false;
                    this.color = color;
                    this.opacity = .5;
                    this.direction = direction;
                    this.location = location;
                    
                    this.shipBlocks = [];
                    
                    // Block class
                    function Block(location, color){
                    	
                        this.location = location;
                        this.color = color;
                        this.damage = 0;
                        
                        this.updateBlock = function() {
                            // doesn't display if block is out of range
                            if (this.location.row < block_count && this.location.col < block_count) {
                                // gets top left point of block to fill
                                var coordinates = RC2XY(this.location, "block");
                                
                                context.beginPath();
                                context.fillStyle = this.color;
                                context.fillRect(
                                    coordinates.x, 
                                    coordinates.y, 
                                    block_size, 
                                    block_size);
                            }
                        };
                    }
                    
                    for (var i = 0; i < this.length; i++) {
                        switch(direction) {
                            case 'left':
                                this.shipBlocks.push(new Block({ 
                                        "row" : location.row,
                                        "col" : location.col - i
                                    }, color));
                                break;
                            case 'right':
                                this.shipBlocks.push(new Block({ 
                                        "row" : location.row,
                                        "col" : location.col + i
                                    }, color));
                                break;
                            case 'up':
                                this.shipBlocks.push(new Block({ 
                                        "row" : location.row - i,
                                        "col" : location.col
                                    }, color));
                                break;
                            case 'down':
                                this.shipBlocks.push(new Block({ 
                                        "row" : location.row + i,
                                        "col" : location.col
                                    }, color));
                                break;
                            default: console.error("Block.initializeBlocks has no direction");
                        }
                    }
                    this.updateShip = function() {
                        // draw whole ship
                        
                        // getting nw and se locations
                        // for left and up this is a misnomer, it would actually be flipped (it's actually easier this way lol)
                        this.nw = RC2XY(location, "block");
                        
                        if (direction == "left" || direction == "up") {
                            this.nw.x += block_size;
                            this.nw.y += block_size;
                        }
                        
                        var lengthAddition = this.length - 1;
                        
                        switch(direction) {
                            case 'left':
                                this.se = RC2XY({
                                    "row" : location.row,
                                    "col" : location.col - lengthAddition
                                }, "block");
                                break;
                            case 'right':
                                this.se = RC2XY({
                                    "row" : location.row,
                                    "col" : location.col + lengthAddition
                                }, "block");
                                break;
                            case 'up':
                                this.se = RC2XY({
                                    "row" : location.row - lengthAddition,
                                    "col" : location.col
                                }, "block");
                                break;
                            case 'down':
                                this.se = RC2XY({
                                    "row" : location.row + lengthAddition,
                                    "col" : location.col
                                }, "block");
                                break;
                            default: console.error("No Direction");
                        }
                        //this.se = RC2XY([this.shipBlocks[this.shipBlocks.length - 1].row, this.shipBlocks[this.shipBlocks.length - 1].col], "block");
                        
                        if (direction == "right" || direction == "down") {
                            this.se.x += block_size;
                            this.se.y += block_size;
                        }
                        
                        // if sunk, fade away
                        if (!this.shipBlocks.length) {
                            this.opacity -= .005;
                            if (this.opacity < .01)
                                this.sunk = true;
                        }
                        
                        if (this.visibility || !this.shipBlocks.length) {
                            // draw ship overlay
                            context.beginPath();
                            context.fillStyle = ("rgba(" + parseInt(this.color.slice(1, 3), 16) + ", " + parseInt(this.color.slice(3, 5), 16) + ", " + parseInt(this.color.slice(5, 7), 16) + ", " + this.opacity + ")");
                            context.fillRect(
                                this.nw.x, 
                                this.nw.y, 
                                this.se.x - this.nw.x, 
                                this.se.y - this.nw.y);
                                
                            // draw underlying blocks
                            for (var i = 0; i < this.shipBlocks.length; i++) {
                                this.shipBlocks[i].updateBlock();
                            }
                        }
                    };
                    
                    
                    this.impact = function(blockIndex, force, forceSource) {
                        var block = this.shipBlocks[blockIndex];
                        block.damage += Math.abs(force);
                        
                        
                        if (block.damage > shipHP) {
                            
                            shotSounds.playRandom(false);
                            if (this.name != "miss_ship") {
                                hitSounds.playRandom(false);
                            } else {
                                missSounds.playRandom(false);
                            }
                            
                            this.shipBlocks.splice(blockIndex, 1);
                            var yerOldeXY = RC2XY(block.location, "block");
                            
                            for(var i = 0; i < fractures; i++) {
                                for (var ii = 0; ii < fractures; ii++) {
                                    var coordinates = {
                                        "x" : yerOldeXY.x + (block_size / fractures * i),
                                        "y" : yerOldeXY.y + (block_size / fractures * ii)
                                    };
                                    players.addParticle(currentGameState == "attack" ? opponent_id : player_id, coordinates, force, forceSource, block_size / fractures, block.color);
                                }
                            }
                            
                            switch(currentGameState) {
                                case "attack":
                                    players.changePegColor(opponent_id, block.location, block.color);
                                    break;
                                case "defend":
                                    players.changePegColor(player_id, block.location, block.color);
                                    break;
                                default: console.log("Unusual gameState for blocks to be impacted");
                           
                            }
                            
                            
                        }
                        return (block.damage > shipHP);
                	};
                    
                }
            }
            
            
            
            
            function Particles() {
                var particles = [];
                
                this.clear = function() {
                    particles = [];
                };
                
                this.addParticle = function(coordinates, force, forceSource, size, color) {
                    particles.push(new Particle(coordinates, force, forceSource, size, color));
                };
                
                this.update = function() {
                    for (var i = 0; i < particles.length; i++) {
                        particles[i].update();
                    }
                };
                
                function Particle(coordinates, force, forceSource, size, color) {
                    this.x = coordinates.x / dimensions;                                                // x at start
                    this.y = coordinates.y / dimensions;                                                // y at start
                    this.size = size;                                                       // size of square particle
                    this.color = color;                                                     // color is inherited from original block
                    this.lifespan = 0;                                                      // lifespan is increased by 1 every frame
                    
                    this.centerX = this.x + (this.size / dimensions / 2);
                    this.centerY = this.y + (this.size / dimensions / 2);
                    
                    // determines slope of force 
                    var particleSlope = (this.centerY - forceSource.y) / (this.centerX - forceSource.x);
            
                    // gets the force angle from the slope: tan-1(m) = theta
                    var forceAngle = Math.atan(Math.abs(particleSlope));
                    
                    // adjusts for negative forces and stuff because tan-1 works best with angles in quadrant I (also less confusing)
                    var forceX = this.centerX < forceSource.x ? -force : force;
                    var forceY = this.centerY < forceSource.y ? -force : force;
                    
                    
                    // scales force as determined by forceScaling basically
                    forceX /= forceScaling;
                    forceY /= forceScaling;
                    
                    
                    this.vx = forceX * Math.cos(forceAngle) * (forceRandomizationFactor * Math.random() + (1 - forceRandomizationFactor)) / dimensions;
                    this.vy = forceY * Math.sin(forceAngle) * (forceRandomizationFactor * Math.random() + (1 - forceRandomizationFactor)) / dimensions;
                    
                    
                    // updates this particle: checks for collisions, updates location, renders and decays the particle
                    this.update = function() {
                        
                        this.checkCollision();
                        
                        this.x += this.vx;
                        this.y += this.vy;  
                        
                        this.vx /= Math.pow(dragForce, this.lifespan / dragForceLifespanScalingDivider);
                        this.vy /= Math.pow(dragForce, this.lifespan / dragForceLifespanScalingDivider);
                        
                        
                        // shitty rounding
                        var round = .0001;
                        this.vx = this.vx < round && this.vx > -round ? 0 : this.vx;
                        this.vy = this.vy < round && this.vy > -round ? 0 : this.vy;
                        
                        
                        context.beginPath();
                        context.fillStyle = ("rgba(" + parseInt(this.color.slice(1, 3), 16) + ", " + parseInt(this.color.slice(3, 5), 16) + ", " + parseInt(this.color.slice(5, 7), 16) + ", " + ((particleFramesOfExistence - this.lifespan + 20) / particleFramesOfExistence) + ")");
                        context.fillRect(
                    	    this.x * dimensions, 
                    	    this.y * dimensions, 
                    	    this.size, 
                    	    this.size);
                    	    
                    	this.lifespan++;
                    	if (this.lifespan > particleFramesOfExistence) {
                    	    var index = particles.indexOf(this);
                    	    particles.splice(index, 1);
                    	}
                    };
                    this.checkCollision = function() {
                        // left side
                        if (this.x < 0 || this.x > 1) {
                            this.vx *= -1;
                        }
                        if (this.y < 0 || this.y > 1) {
                            this.vy *= -1;
                        }
                    };
                }
            }
            
            
            
            
            
            
            
            function Tracker() {
                var pegs = [];
                
                this.addShot = function(location) {
                    pegs.push({
                        "location" : location,
                        "color" : "White",
                        "age" : 0
                    });
                };
                
                this.isThereAPegHere = function(location) {
                    for (var i = 0; i < pegs.length; i++) {
                        if (pegs[i].location.row == location.row && pegs[i].location.col == location.col) {
                            return true;
                        }
                    }
                    return false;
                };
                
                this.changeColor = function(location, color) {
                    var index = getIndex(location);
                    pegs[index].color = color;
                };
                
                this.age = function() {
                    for (var i = 0; i < pegs.length; i++) {
                        pegs[i].age++;
                    }
                };
                
                this.update = function() {
                    for (var i = 0; i < pegs.length; i++) {
                        
                        var coordinates = RC2XY(pegs[i].location, "peg");
                            
                        context.beginPath();
                        context.fillStyle = pegs[i].color;
                        context.fillRect(
                            coordinates.x, 
                            coordinates.y, 
                            peg_size, 
                            peg_size);
                    }
                };
                
                function getIndex(location) {
                    for (var i = pegs.length - 1; i >= 0; i--) {
                        if (pegs[i].location.row == location.row && pegs[i].location.col == location.col) { return i; }
                    }
                    console.error("Couldn't find peg at location: " + location.row, location.col);
                }
            }
        };
        
        this.updateCircles = function() {
            
            // moved down and there is no other player
            if (loadPercentage.opacity < 0) {
                // if moved down and only one person
                if (people.length < 2) {
                    if (sideShift < .5) {
                        sideShift += .001;
                    }
                // if moved down and two people and sideshift is nonzero
                } else if (sideShift > 0) {
                    sideShift -= .01 * (.5 - Math.pow(sideShift, 3));
                // if moved down and two people and sideshift is zero and there is no startbutton
                } else if (!document.getElementById("startButton")) {
                    elements.addElement(
                        "button",
                        1 / 2 ,
                        6 / 8, 
                        200, 
                        20, 
                        'Start Match', 
                        'startButton'
                    );
                }
                sideShift = Math.round(sideShift * 1000) / 1000;
                if (sideShift < 0) sideShift = 0;
                if (sideShift && Math.random() > .5) { randomStuff.addRandomSpeck(); }
            }
            
            // for each player
            for (var i = 0; i < people.length; i++) {
                var circleX = (i) + (.5);
                var circleY = Math.pow(loadPercentage.opacity, 3) + .5;
                //              if two people               if second boat 
                var speedy = people.length < 2 ? sideShift : !i ? .5 : (sideShift < .01 ? .5 - (sideShift * 20) : 0);
                
                imageStack.repositionAnimation(i == 0 ? "excellent ship" : "terrified ship", circleX - speedy, circleY);
                try {
                    drawText(
                        ((circleX - (((2 * i) + 1) * (speedy) / 2)) * (isLandscape ? dimensions + overflowSize : dimensions)), 
                        (circleY * (isLandscape ? dimensions : dimensions + overflowSize)) + 100, 
                        people[i].player_name, 
                        64, 
                        "verdana", 
                        "white");
                }
                catch (exception) {
                    console.error(exception.name + ", i = " + i);
                }
            }
            

        };
    }
    
    function drawText(centerX, centerY, text, height, fontStyle, color) {
        context.beginPath();
        context.fillStyle = color;
        context.font = height + "px " + fontStyle;
        context.fillText(
            text, 
            centerX - (context.measureText(text).width / 2), 
            centerY + (height / 2)
        );
    }
    
    function setBackground(color) {
        context.beginPath();
        context.fillStyle = color;
        context.fillRect(
            -1,
            -1,
            dimensions + (isLandscape ? overflowSize : 0) + 1, 
            dimensions + (isLandscape ? 0 : overflowSize + 1) + 1
        );
    }
    
    var loadPercentage = new LoadPercentage();
    function LoadPercentage() {
        var waves = 4;
        var waveHeight = dimensions / 20;
        var waveOffset = 0;
        var lightOffset = 0;
        var waveIncrement = 20;
        this.opacity = 1;
        
        this.update = function(percent) {
            if (!this.opacity) return;
            
            var se = {
                "x" : (isLandscape ? dimensions + overflowSize : dimensions),
                "y" : (isLandscape ? dimensions : dimensions + overflowSize)
            };
            
            if (Math.floor(percent)) {
                this.opacity -= .01;
                this.opacity = Math.round(this.opacity * 100) / 100;
                
                if (this.opacity == .01) {
                    
                    var heights = 20;
                    var widths = 200;
                    elements.addElement(
                        "textarea",
                        1 / 2,
                        3 / 8, 
                        widths, 
                        heights, 
                        'Enter Username', 
                        'usernameField'
                    );
                    
                    elements.addElement(
                        "textarea",
                        1 / 2,
                        4 / 8, 
                        widths, 
                        heights, 
                        'Enter Room Number', 
                        'game_nameField'
                    );
                    
                        
                    elements.addElement(
                        "button",
                        1 / 2 ,
                        5 / 8, 
                        widths, 
                        heights, 
                        'Create / Enter Room', 
                        'enterButton'
                    );
                }
                
                setBackground("rgba(65, 105, 225, " + this.opacity + ")");
                    
                drawText(
                    ((isLandscape ? dimensions + overflowSize : dimensions) / 2), 
                    ((isLandscape ? dimensions : dimensions + overflowSize) / 2) - ((1 - this.opacity) * dimensions), 
                    Math.round(Math.round(percent * 100)) + "%", 
                    128, 
                    "verdana", 
                    "rgba(255, 255, 255, " + this.opacity + ")"
                );
                    
                    
                return;
            }
            
            
            setBackground("#0d0d40");
            
            
            se.x *= 2;
            se.y2 = se.y * (1 - percent) - waveHeight;
            
            
            waveOffset = waveOffset + waveIncrement;
            if (waveOffset >= 2 * se.x / waves) {
                waveOffset = 0;
            }
            
            if (!lightOffset) {
                lightOffset = -(se.x / 2);
            }
            lightOffset = lightOffset + (waveIncrement / 2);
            if (lightOffset >= -(se.x / 2) + 2 * se.x / waves) {
                lightOffset = -(se.x / 2);
            }
            
            context.beginPath();
            context.moveTo(lightOffset, se.y);
            context.lineTo(lightOffset, se.y2);
            
            for (var i = 0; i < waves; i++) {
                context.quadraticCurveTo(
                    se.x * (i + 1 / 2) / waves + lightOffset, se.y2 + (i % 2 ? waveHeight : -waveHeight),
                    se.x * (i + 1 / 1) / waves + lightOffset, se.y2
                    );
            }
            context.lineTo(se.x + lightOffset, se.y2);
            context.lineTo(se.x + lightOffset, se.y);
            context.lineTo(-waveOffset, se.y);
            
            context.closePath();
            context.fillStyle = "paleturquoise";
            context.fill();
            
            
            context.beginPath();
            context.moveTo(-waveOffset, se.y);
            context.lineTo(-waveOffset, se.y2);
            
            for (var i = 0; i < waves; i++) {
                context.quadraticCurveTo(
                    se.x * (i + 1 / 2) / waves - waveOffset, se.y2 + (i % 2 ? waveHeight : -waveHeight),
                    se.x * (i + 1 / 1) / waves - waveOffset, se.y2
                    );
            }
            context.lineTo(se.x - waveOffset, se.y2);
            context.lineTo(se.x - waveOffset, se.y);
            context.lineTo(-waveOffset, se.y);
            
            context.closePath();
            context.fillStyle = "royalblue";
            context.fill();
            
            
            drawText(
                (isLandscape ? dimensions + overflowSize : dimensions) / 2, 
                (isLandscape ? dimensions : dimensions + overflowSize) / 2, 
                Math.round(Math.round(percent * 100)) + "%", 
                128, 
                "verdana", 
                "white"
            );
            
            
            
            
        };
    }
    
    function drawMinimap(user_id) {
        context.save();
        
        context.transform(
            Math.min(.5, overflowSize / dimensions), 
            0, 
            0, 
            Math.min(.5, overflowSize / dimensions), 
            isLandscape ? dimensions + overflowSize - Math.min(dimensions / 2, overflowSize) : 0, 
            isLandscape ? 0 : dimensions + overflowSize - Math.min(dimensions / 2, overflowSize));
            
        setBackground("white");
        players.update(user_id);
        
        context.restore();
    }
    
    
    // loops the loop
    function loop(){
        time = new Date().getTime();
        
        
        frame++;
        
        clearCanvas();
        
        switch(currentGameState) {
            
            case 'initialization': ////////////////////
                
                
                
                imageStack.update();
                crosshair.update();
                
                var audioLoadProgress = (
                    shotSounds.allAudioLoaded() + 
                    hitSounds.allAudioLoaded() + 
                    missSounds.allAudioLoaded() + 
                    dingSounds.allAudioLoaded() +
                    pingSound.allAudioLoaded()
                    ) / 5;
            	
                if (
                    communicator.socket.isConnected &&
                    (Math.floor(audioLoadProgress) && 
                    Math.floor(imageStack.allImagesLoaded()) ||
                    frame >= maxLoadTime)
                    ) {
                    if (frame >= maxLoadTime) {
                        console.log("Resource Loading set to JIT.");
                    }
                    console.log("current frame: " + frame);
                    console.log("Done");
                    nextState();
                } else {
                    console.log("audio: " + Math.round(audioLoadProgress * 100) + "%, images: " + Math.round(imageStack.allImagesLoaded() * 100) + "%, socket: " + communicator.socket.isConnected);
                    loadPercentage.update(
                        Math.min(
                            frame / maxLoadTime, 
                            (audioLoadProgress + imageStack.allImagesLoaded() + communicator.socket.isConnected) / 3
                        )
                    );
                    if (frame >= maxLoadTime) {
                        drawText(
                            (isLandscape ? dimensions + overflowSize : dimensions) / 2, 
                            3 * (isLandscape ? dimensions : dimensions + overflowSize) / 4, 
                            "Difficulty Connecting to Server",
                            64,
                            "verdana",
                            "red");
                        
                    }
                }
                
                
                
                break;
            case 'connection': ////////////////////
            
                setBackground("#0d0d40");
                
                drawText(
                    ((isLandscape ? dimensions + overflowSize : dimensions) / 2),
                    ((isLandscape ? dimensions : dimensions + overflowSize) / 6) + 2,
                    "qBattleship",
                    "64",
                    "verdana",
                    "grey"
                );
                drawText(
                    ((isLandscape ? dimensions + overflowSize : dimensions) / 2),
                    ((isLandscape ? dimensions : dimensions + overflowSize) / 6),
                    "qBattleship",
                    "64",
                    "verdana",
                    "white"
                );
                
                
                elements.vibrationUpdate();
                loadPercentage.update(true);
                imageStack.update();
                crosshair.update();
                
                if (Math.random() > .85) {
                    randomStuff.addRandomSpeck();
                }
                
                randomStuff.update();
                
                
                break;
            case 'opponent': ////////////////////
                
                setBackground("#08082b");
                setBackground("rgba(13, 13, 64, " + loadPercentage.opacity + ")");
                
                
                // going down again (reusing variables?????? nani kore)
                
                drawText(
                    ((isLandscape ? dimensions + overflowSize : dimensions) / 2),
                    (1.5 * Math.pow(loadPercentage.opacity, 3) - .5) * ((isLandscape ? dimensions : dimensions + overflowSize) / 6) + 2,
                    "qBattleship",
                    "64",
                    "verdana",
                    "grey"
                );
                drawText(
                    ((isLandscape ? dimensions + overflowSize : dimensions) / 2),
                    (1.5 * Math.pow(loadPercentage.opacity, 3) - .5) * ((isLandscape ? dimensions : dimensions + overflowSize) / 6),
                    "qBattleship",
                    "64",
                    "verdana",
                    "white"
                );
                
                
                if (loadPercentage.opacity > 0) {
                    loadPercentage.opacity -= .005;
                }
                
                if (!players.count()) {
                    console.log("Waiting for server.. ");
                }
                
                players.updateCircles();
                
                
                if (Math.random() > .95) {
                    randomStuff.addRandomSpeck();
                }
                
                imageStack.update();
                randomStuff.update();
                
                context.beginPath();
                context.fillStyle = "white";
                context.font = "36px verdana"; 
                context.fillText(
                    "Room: " + game_name, 
                    20, 
                    50
                );
                context.closePath();
                
                
                if (!(frame % 200) && (players.count() == 1)) {
                    pingSound.play("ping");
                }
                
                break;
            case 'setup': ////////////////////
                
            	menu.update();
            	crosshair.update();
            	imageStack.update();
            	players.update(player_id);
                
                
                break;
            case 'attack': ////////////////////
                
                
                menu.update();
                imageStack.update();
                players.update(opponent_id);
                
                drawMinimap(player_id);
                
                crosshair.update();
                if (players.shipCount(opponent_id) == players.salvoSize && players.shotsIncoming(opponent_id) == players.salvoSize && turn) {
                    console.log("I fired shot");
                    players.fireShots(opponent_id);
                    turn = false;
                }
                break;
            case 'defend': ////////////////////
                
                
                
                menu.update();
                players.update(player_id);
                imageStack.update();
                
                drawMinimap(opponent_id);
                
                crosshair.update();
                
                if (players.shotsIncoming(player_id) == players.salvoSize && !turn) {
                    console.log("opponent fired shot");
                    players.fireShots(player_id);
                    turn = true;
                }
                
                
                break;
            case 'endgame':
                
                setBackground("#0d0d40");
                
                elements.removeAllElements();
                imageStack.clearActive();
                
                randomStuff.update();
                imageStack.update();
                crosshair.update();
                
                if (winner_id == player_id) {
                    drawText(
                        ((isLandscape ? dimensions + overflowSize : dimensions) / 2) - 2,
                        ((isLandscape ? dimensions : dimensions + overflowSize) / 2) - 2,
                        "You Won!",
                        128,
                        "verdana",
                        "yellow"
                    );
                    drawText(
                        ((isLandscape ? dimensions + overflowSize : dimensions) / 2),
                        ((isLandscape ? dimensions : dimensions + overflowSize) / 2),
                        "You Won!",
                        128,
                        "verdana",
                        "blue"
                    );
                    drawText(
                        ((isLandscape ? dimensions + overflowSize : dimensions) / 2) + 2,
                        ((isLandscape ? dimensions : dimensions + overflowSize) / 2) + 2,
                        "You Won!",
                        128,
                        "verdana",
                        "red"
                    );
                } else {
                    drawText(
                        ((isLandscape ? dimensions + overflowSize : dimensions) / 2),
                        ((isLandscape ? dimensions : dimensions + overflowSize) / 2),
                        "You Lost!",
                        128,
                        "verdana",
                        "black"
                    );
                }
                
                if (Math.random() > .95) {
                    var size = Math.random() * block_size;
                    randomStuff.addCircle(
                        Math.random() * (isLandscape ? dimensions + overflowSize : dimensions),
                        Math.random() * (isLandscape ? dimensions : dimensions + overflowSize),
                        size * 2,
                        Math.round(size / (block_size / 10)),
                        false);
                }
                break;
    	}
    	
    	transition.update();
    	
    	
        var currentTime = new Date().getTime();
        var framesPehSecond = Math.round(1000 / (currentTime - time));
        var color = "black";
        if (currentGameState == "initialization" || currentGameState == "connection" || currentGameState == "opponent" || currentGameState == "endgame") {
            color = "white";
        }
        framesPehSecond = framesPehSecond == Infinity ? "Too Many" : framesPehSecond;
        drawText(
            (dimensions + (isLandscape ? overflowSize : 0)) - 50, 
            10,
            framesPehSecond,
            12,
            "verdana", 
            color
        );
        time = currentTime;
    }
    
    function refresh() {
        
        var width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;
        
        var height = window.innerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight;
        
        isLandscape = width >= height;
        
        var canvasHeight = Math.round(Math.min(height, width * overflowRatio) * windowToCanvasRatio);
        var canvasWidth = Math.round(Math.min(width, height * overflowRatio) * windowToCanvasRatio);
        
        canvas.height = canvasHeight;
        canvas.width = canvasWidth;
        canvas.style.height = canvasHeight + 'px';
        canvas.style.width = canvasWidth + 'px';
        dimensions = Math.min(canvasHeight, canvasWidth);
        overflowSize = Math.abs(canvasHeight - canvasWidth);
        
        grid_size = ((dimensions - grid_padding * 2) / block_count);
        block_size = .4 * grid_size;
        block_padding = .6 * grid_size;
        
        chat.update();
        elements.update();
        
    }


    // clears the canvas basically
    function clearCanvas() { context.clearRect(-.5, -.5, (isLandscape ? dimensions + overflowSize : dimensions), (isLandscape ? dimensions : dimensions + overflowSize)); }
    
    
    // gets new mouse position on move
    function onMouseMoveHandler(event){ 
        this.event = event ? event : window.event;
    	var rectangle = canvas.getBoundingClientRect();
    	if (canvasMovesWithScroll) {
    	    mouseX = (event.pageX ? event.pageX : event.clientX) - rectangle.left;
    	    mouseY = (event.pageY ? event.pageY : event.clientY) - rectangle.top;
    	}
    	else {
        	mouseX = Math.round(event.x - rectangle.left);
        	mouseY = Math.round(event.y - rectangle.top);
    	}
    }
    
    // creates blocks where user clicks
    function onClickHandler(event) { 
        switch(currentGameState) {
            case "connection": 
                break;
            case 'setup':
                if (players.queueCount(player_id)) {
                    multipleSelectionAction(XY2RC({"x" : mouseX / dimensions, "y" : mouseY / dimensions}), pointDirection, players.getQueuedShipLength(player_id, 0), 'add');
                    if (!players.queueCount(player_id)) {
                        communicator.sendData("place_ships");
                    }
                }
                else {
                    console.log("There are no ships remaining");
                }
                break;
            case 'attack':
                if (players.shotsRemaining(opponent_id)) {
                    var location = XY2RC({"x" : mouseX / dimensions, "y" : mouseY / dimensions});
                    if (location && !players.isThereAPegHere(opponent_id, location)) {
                        players.queueShot(opponent_id, mouseX / dimensions, mouseY / dimensions);
                        players.addPeg(opponent_id, location);
                        players.changePegColor(opponent_id, location, "Red");
                    }
                    if (!players.shotsRemaining(opponent_id)) {
                        communicator.sendData("make_shot");
                    }
                }
                break;
            case "endgame":
                var size = Math.random() * block_size;
                randomStuff.addCircle(
                    mouseX,
                    mouseY,
                    size * 2,
                    Math.round(size / (block_size / 10)),
                    false);
                break;
        }
    }
    
    function onMouseUpHandler(event) { 
        if (event.button == 2) { onRightClickHandler(event); }
    }
    
    
    // change direction when right clicked
    function onRightClickHandler(event) { 
        event.preventDefault(event);
        switch (pointDirection) {
            case 'right':
                pointDirection = 'down';
                break;
            case 'down':
                pointDirection = 'left';
                break;
            case 'left':
                pointDirection = 'up';
                break;
            case 'up':
                pointDirection = 'right';
                break;
            default: console.error("pointDirection was not a direction?");
        }
        return false;
    }
    
    function onKeyHandler(event) {
        if (event.keyCode == 32 && event.target == document.body) {
            if (currentGameState == "endgame") {
                randomStuff.applyForce({"x" : (mouseX / dimensions), "y" : (mouseY / dimensions)});
            }
            event.preventDefault();
            return false;
        }
    }
    
    
    /** Mobile Inputs **/ 
    
    var mainTouch;
    
    function touchHandler(event) {
        if(event.touches) {
            var rectangle = canvas.getBoundingClientRect();
            if (canvasMovesWithScroll) {
                mouseX = event.touches[0].pageX - rectangle.left;
                mouseY = event.touches[0].pageY - rectangle.top;
            }
            else {
                mouseX = event.touches[0].clientX - rectangle.left;
                mouseY = event.touches[0].clientY - rectangle.top - (dimensions / 6);
                mainTouch = event.touches[0].identifier;
            }
            event.preventDefault();
            return false;
        }
    }
    
    function touchEndHandler(event) {
        if (event.changedTouches[0].identifier != mainTouch) {
            onRightClickHandler(event);
            return;
        }
        else {
            onClickHandler(event);
            mouseX = -500;
            mouseY = -500;
            mainTouch = null;
        }
    }
    
    
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////// BALL



    // class for that ball that randomly flies around
    function Ball(){
        // random starting point and speed
        this.x = Math.random() * (.6 * dimensions)  + (.2 * dimensions);
        this.y = Math.random() * (.6 * dimensions)  + (.2 * dimensions);
        this.vx = Math.random() * (.01 * dimensions) + (.01 * dimensions);
        this.vy = Math.random() * (.01 * dimensions) + (.01 * dimensions);
        
        this.radius = (.02 * dimensions);
        this.color = 'rgb(100,100,100)';
        
        this.update = function() {
            this.y += this.vy;
            this.x += this.vx;
            if(ball.y + ball.radius > (isLandscape? dimensions : dimensions + overflowSize) || ball.y - ball.radius < 0) { ball.vy *= -1; }
            if(ball.x + ball.radius > (isLandscape? dimensions + overflowSize : dimensions) || ball.x - ball.radius < 0) { ball.vx *= -1; }	
            context.beginPath();
            context.fillStyle = this.color;
            context.arc(this.x, this.y, this.radius, 0, Math.PI*2, true);
            context.fill();
            
            if (imageStack.animationsCount() < 1000) {
                if (Math.random() > .95) { imageStack.activateSpriteImage("testo", this.x, this.y, true, false, 2 * Math.PI * Math.random(), 3); }
            }
            
        };
    }
    
    function AudioStack(volume) {
        var sounds = [];
        this.shuffle = false;
        this.volume = volume;
        
        this.addAudioToLibrary = function(name, sourceURL) {
            var audioObject = new AudioFile(name, sourceURL, this.volume);
            sounds.push(audioObject);
        };
        
        this.allAudioLoaded = function() {
            if (!sounds.length) return true;
            var filesLoaded = 0;
            for (var i = 0; i < sounds.length; i++) {
                if (sounds[i].audio.readyState >= 4) {
                    filesLoaded++;
                }
            }
            return filesLoaded / sounds.length;
        };
        
        this.clearActive = function() {
            for (var i = 0; i < sounds.length; i++) {
                sounds[i].audio.pause();
                sounds[i].audio.currentTime = 0;
            }
        };
        
        this.playRandom = function(shuffle) {
            var file = sounds[Math.floor(sounds.length * Math.random())];
            activate(file, this.volume);
            this.shuffle = shuffle;
        };
        
        
        this.play = function(name) {
            activate(sounds[getIndex(name)], this.volume);
        };
        
        this.load = function() {
            for (var i = 0; i < sounds.length; i++) {
                sounds[i].audio.load();
            }
        };
        
        function activate(file, volume) {
            try {
                file.audio.volume = volume;
                file.audio.currentTime = 0;
                file.audio.play();
                console.log("Now Playing: " + file.name);
            }
            catch (exception) {
                console.error(exception.name + ": " + exception.message);
            }
        }
        
        function getIndex(name) {
            for (var i = 0; i < sounds.length; i++) {
                if (sounds[i].name == name) {
                    return i;
                }
            }
        }
        
        this.update = function() {
            if (this.shuffle) {
                var playing = false;
                for (var i = 0; i < sounds.length; i++) {
                    if (!sounds[i].audio.paused) {
                        if (sounds[i].audio.volume != this.volume) {
                            sounds[i].audio.volume = parseFloat(Math.round((sounds[i].audio.volume + (sounds[i].audio.volume < this.volume ? .001 : -.001)) * 1000) / 1000);
                        }
                        playing = true;
                    }
                }
                if (!playing) {
                    activate(sounds[Math.floor(sounds.length * Math.random())], this.volume);
                }
            }
        };
        
        function AudioFile(name, sourceURL, volume) {
            
            this.name = name;
            this.audio = new Audio(sourceURL);
            this.audio.volume = volume;
            this.audio.load();
            
        }
    }
    
    // stack of currently loaded images
    function ImageStack() {
        var images = [];
        var animations = [];
        
        this.repositionAnimation = function(name, x, y) {
            var index = getAnimatingIndex(name);
            if (index == null) {
                console.log("Couldn't find animation: " + name);
                console.log(animations);
                return;
            }
            animations[index].changePosition(x, y);
        };
        
        this.animationsCount = function() {
            return animations.length;
        };
        
        this.allImagesLoaded = function() {
            var filesLoaded = 0;
            for (var i = 0; i < images.length; i++) {
                if (images[i].imageObject.loaded) { 
                    filesLoaded++;
                }
            }
            return filesLoaded / images.length;
        };
        
        this.addImageToLibrary = function(name, imageURL, frameCount, magnification) {
            var spriteImageObject = new SpriteImage(name, imageURL, frameCount, magnification, 0, 0, false, false, false, false, 1);
            images.push(spriteImageObject);
        };
        
        this.update = function() {
            for (var i = 0; i < animations.length; i++) {
                animations[i].update();
                if (animations[i].completed) { animations.splice(i, 1); }
        }};
        
        this.activateSpriteImage = function(name, centerX, centerY, repeating, crop, rotate, framesPerFrame) {
            var original = images[getIndex(name)];
            animations.push(new SpriteImage(
                name,
                original.imageObject.src,
                original.imageObject.frameCount,
                original.magnification,
                centerX,
                centerY,
                true,
                repeating,
                crop,
                rotate,
                framesPerFrame
            ));
        };
        
        this.clearActive = function() {
            animations = [];
        };
        
        function getIndex(name) {
            for (var i = 0; i < images.length; i++) {
                if (images[i].name == name) {
                    return i;
                }
            }
        }
        
        // best used on looping animations
        function getAnimatingIndex(name) {
            for (var i = 0; i < animations.length; i++) {
                if (animations[i].name == name) {
                    return i;
                }
            }
        }
        
        function SpriteImage(name, sourceURL, frameCount, magnification, centerX, centerY, activated, repeating, crop, rotate, framesPerFrame) {
            this.name = name;
            
            this.repeating = repeating;
            this.completed = false;
            this.activated = activated;
            this.currentFrame = 0;
            this.framesPerFrame = framesPerFrame;
            this.currentFrameOfFrame = 0;
            this.magnification = magnification;
            this.x = centerX / (isLandscape ? dimensions + overflowSize : dimensions);
            this.y = centerY / (isLandscape ? dimensions : dimensions + overflowSize);
            
            this.imageObject = new Image();
            this.imageObject.loaded = false;
            this.imageObject.frameCount = frameCount;
            
            // in here, this = imageObject
            this.imageObject.onload = function() {
                this.loaded = true;
                this.frameCount = this.frameCount == 'auto' ? this.width / this.height : this.frameCount;
                if (!Number.isInteger(this.frameCount)) { console.error("'auto' only works on spritesheets with square sprites. Ratio is : " + this.frameCount); }
                this.frameHeight = this.height;
                this.frameWidth = this.width / this.frameCount;
            };
            
            // source is added after onload() is declared in case image is stored in buffer
            this.imageObject.src = sourceURL;
            
            this.changePosition = function(x, y) {
                this.x = x;
                this.y = y;
            };
            
            this.remove = function() {
                this.completed = true;
            };
            
            this.update = function() {
                if (!this.activated) return;
                if (!this.imageObject.loaded) {
                    // image hasn't loaded yet
                    return;
                }
                
                var magnificationNow = (this.magnification * dimensions / 1000);
                
                var sourceX = this.imageObject.frameWidth * this.currentFrame, 
                    sourceY = 0, 
                    sourceWidth = this.imageObject.frameWidth, 
                    sourceHeight = this.imageObject.frameHeight, 
                    destinationX = (this.x * (isLandscape ? dimensions + overflowSize : dimensions)) - (this.imageObject.frameWidth * (magnificationNow) / 2), 
                    destinationY = (this.y * (isLandscape ? dimensions : dimensions + overflowSize)) - (this.imageObject.frameHeight * (magnificationNow) / 2), 
                    destinationWidth = this.imageObject.frameWidth * magnificationNow, 
                    destinationHeight = this.imageObject.frameHeight * magnificationNow;
                    
                if (crop || rotate) { context.save();}
                    
                if (crop) {
                    context.beginPath();
                    context.arc(this.x * (isLandscape ? dimensions + overflowSize : dimensions), this.y * (isLandscape ? dimensions : dimensions + overflowSize), crop, 0, Math.PI * 2, true);
                    context.closePath();
                    context.clip();
                }
                
                if (rotate) { 
                    context.translate(this.x * (isLandscape ? dimensions + overflowSize : dimensions), this.y * (isLandscape ? dimensions : dimensions + overflowSize));
                    destinationX = - (this.imageObject.frameWidth * magnificationNow / 2);
                    destinationY = - (this.imageObject.frameHeight * magnificationNow / 2);
                    context.rotate(rotate); 
                }
                
                context.drawImage(this.imageObject, sourceX, sourceY, sourceWidth, sourceHeight, destinationX, destinationY, destinationWidth, destinationHeight);
                
                if (crop) {
                    context.beginPath();
                    context.arc(centerX, centerY, crop, 0, Math.PI * 2, true);
                    context.clip();
                    context.closePath();
                }
                
                if (crop || rotate) { context.restore(); }
                
                
                if (this.currentFrameOfFrame < this.framesPerFrame - 1) { this.currentFrameOfFrame++; } 
                else {
                    this.currentFrame++;
                    this.currentFrameOfFrame = 0;
                    if (this.currentFrame > this.imageObject.frameCount - 1) {
                        this.currentFrame = 0;
                        if (!this.repeating) { this.remove(); }
                    }
                }
            };
        }
    }
    
    function HTMLElements() {
        this.elements = [];

        this.addElement = function(type, x, y, width, height, text, identifier) {
            var object = new Element(type, x, y, width, height, text, identifier);
            this.elements.push(object);
            this.update();
        };
        
        function Element(type, x, y, width, height, text, identifier) {
            this.x = x;
            this.y = isMobile ? (Math.atan(10 * y - 5) + (Math.PI / 2)) / Math.PI : y;
            this.width = isMobile ? canvas.width * .8 : width;
            this.height = isMobile ? canvas.height * .2 : height;
            this.id = identifier;
            this.vibrationFrames = 0;
            
            
            this.newElement = document.createElement(type);
            document.getElementById('canvasContainer').children[0].appendChild(this.newElement);
            
            this.newElement.id = identifier;
            this.newElement.style.position = 'absolute';
            this.newElement.style.resize = 'none';
            this.newElement.style.textAlign = 'center';
            if (isMobile) {
                this.newElement.style.fontSize = "64px";
            }
            
            
            switch(type) {
                case "textarea":
                    this.newElement.placeholder = text;
                    
                    //this.newElement.addEventListener('mousedown', elements.mouseDownOnTextarea);
                    //this.newElement.addEventListener('keydown', elements.keyDownOnTextarea);
                    
                    break;
                case "button":
                    this.newElement.innerHTML = text;
                    
                    this.newElement.addEventListener('click', elements.buttonClick);
                    
                    break;
                default: console.log("Element type " + type + " not recognized");
            }
            
            
            this.update = function() {
                
                var vibrationHorizontalOffset = 0;
                if (this.vibrationFrames) {
                    this.vibrationFrames--;
                    vibrationHorizontalOffset = (this.vibrationFrames % 2) ? this.vibrationFrames : -this.vibrationFrames;
                    this.newElement.style.backgroundColor = "rgb(255, " + (255  * (1 - (this.vibrationFrames / 20))) + ", " + (255 * (1 - (this.vibrationFrames / 20))) + ")";

                }
                // 10 for the scrollbar
                if (isMobile) {
                    this.newElement.style.top = canvas.offsetTop + (canvas.height * this.y) - (canvas.height * .1) + 'px';
                    this.newElement.style.left = canvas.offsetLeft + (canvas.width * this.x) - (canvas.width * .4) + vibrationHorizontalOffset + 'px';
                    this.newElement.style.width = canvas.width * .8 + 'px';
                    this.newElement.style.height = (isLandscape ? canvas.height * .2 : canvas.width * .2) + 'px';
                } else {
                    var windowWidth = window.innerWidth - 10;
                    var windowHeight = window.innerHeight;
                    this.newElement.style.top = (windowHeight * this.y - (this.height / 2)) + 'px';
                    this.newElement.style.left = (windowWidth * this.x - (this.width / 2)) + vibrationHorizontalOffset + 'px';
                    this.newElement.style.width = this.width + 'px';
                    this.newElement.style.height = this.height + 'px';
                }
            };
            this.update();
        }
        
        this.update = function() {
            for (var i = 0; i < this.elements.length; i++) {
                this.elements[i].update();
            }
        };
        
        this.vibrationUpdate = function() {
            for (var i = 0; i < this.elements.length; i++) {
                if (this.elements[i].vibrationFrames) {
                    this.elements[i].update();
                }
            }
        }
        
        this.removeAllElements = function() {
            var elementsToRemove = this.elements.length;
            for (var i = 0; i < elementsToRemove; i++) {
                var child = document.getElementById(this.elements[0].id);
                child.parentElement.removeChild(child);
                this.elements.splice(0, 1);
            }
        };
        
        this.buttonClick = function(event) {
            switch(event.target.id) {
                case "enterButton":
                    
                    player_name = document.getElementById('usernameField').value;
                    game_name = document.getElementById('game_nameField').value;
                    
                    if (player_name == '') {
                        for (var i = 0; i < elements.elements.length; i++) {
                            if (elements.elements[i].id == "usernameField") {
                                elements.elements[i].vibrationFrames += 20;
                                console.log(elements.elements[i].vibrationFrames);
                                break;
                            }
                        }
                    } else if (game_name == '') {
                        for (var i = 0; i < elements.elements.length; i++) {
                            if (elements.elements[i].id == "game_nameField") {
                                elements.elements[i].vibrationFrames += 20;
                                break;
                            }
                        }
                    } else {
                        
                        communicator.sendData("join_game");
                            
                        nextState(); 
                        elements.removeAllElements();
                    }
                    
                    break;
                case "startButton":
                    
                    if (players.count() > 1) {
                        communicator.sendData("start_game");
                        event.target.innerHTML = "Waiting for Opponent";
                    } else {
                        console.error("Has not yet received game information from server! (join_game packet)");
                    }
                    
                    
                    
                    
                    break;
                    
                case "restartButton":
                    location.reload();
                    break;
                default: console.error("Button Type not recognized: " + event.target.id);
            }
            
        };
        
        this.mouseDownOnTextarea = function(event) {
            
            var x = event.target.offsetLeft - event.clientX,
                y = event.target.offsetTop - event.clientY;
            function drag(event) {
                event.target.style.left = event.clientX + x + 'px';
                event.target.style.top = event.clientY + y + 'px';
            }
            function stopDrag() {
                document.removeEventListener('mousemove', drag);
                document.removeEventListener('mouseup', stopDrag);
            }
            document.addEventListener('mousemove', drag);
            document.addEventListener('mouseup', stopDrag);
        };
        
        this.keyDownOnTextarea = function(event) {
            if (event.keyCode == 13) {
                var value = event.target.value;
                console.log("user entered " + value + " in " + event.target.id);
                event.target.value = '';
                event.preventDefault();
                return false;
            }
        };
    }
    
    
    
    
}
