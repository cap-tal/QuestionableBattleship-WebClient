window.addEventListener('resize', reresize); //window.addEventListener('resize', () => { });
window.addEventListener('focus', reresize);
window.addEventListener("load", reresize);
window.addEventListener("scroll", reresize);

function reresize() {
    
    var screenHeight = window.innerHeight
    || document.documentElement.clientHeight
    || document.body.clientHeight;
    
    var viewTop = document.body.scrollTop;
    var viewBottom = viewTop + screenHeight;
    
    var containers = document.getElementsByClassName("container");
    for (var i = 0; i < containers.length; i++) {
        containers[i].style.height = (screenHeight / containers[i].dataset.divider) + "px";
        
        var divTop = containers[i].offsetTop;
        var divBottom = divTop + containers[i].offsetHeight;
        
        if (divTop < viewBottom && divBottom > viewTop) {
            
            var distanceFromBottomOfScreen = (((divTop + divBottom) / 2) - (viewBottom)) / screenHeight;
            
            // 0% means top of of image, 100% means bottom of image
            containers[i].style.backgroundPositionY = (distanceFromBottomOfScreen + 1.5) * 50 + "%";
            
            var s = window.getComputedStyle(containers[i]).getPropertyValue("background");
            var image = new Image();
            image.src = s.slice(s.indexOf("url(") + 5, Math.max(s.indexOf(".jpg"), s.indexOf(".png"), s.indexOf(".bmp")) + 4);
            
            var heightDifference = Math.max(1.5 * containers[i].offsetHeight / (containers[i].offsetWidth * image.height / image.width), 1);
            containers[i].style.backgroundSize = 100 * heightDifference + "%";
            
        }
        
        
    }
}