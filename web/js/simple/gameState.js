
var noOp = function(){};

var readyState = {
    CONNECTING: 0,
    OPEN: 1,
    CLOSING: 2,
    CLOSED: 3,
};

function Classic(gameName) {
    this.error = null;

    this.playerName = "";
    this.playerId = null;
    this.playerShips = [];

    this.board = new Array();
    for (var i = 0; i < this.BOARD_DIMS.rows; ++i) {
        this.board.push(new Array());
        for (var j = 0; j < this.BOARD_DIMS.cols; ++j) {
            this.board[i].push({ "id": null,
                "hit": null,
                "color": null });
        }
    }

    this.opponentName = "";
    this.opponentId = null;
    this.opponentShips = [];

    // null -> nothing, false -> miss, true -> hit
    this.tracking = new Array();
    for (var i = 0; i < this.BOARD_DIMS.rows; ++i) {
        this.tracking.push(new Array());
        for (var j = 0; j < this.BOARD_DIMS.cols; ++j) {
            this.tracking[i].push(null);
        }
    }

    this.name = gameName;
    this.id = null;

    this.ws = null;
    this.lastMsg = null;

    this.state = null; // str -> phase name
    this.turn = null; // Bool: true -> my turn, false -> opponent turn
}

Classic.prototype.SHIP_INFO = [
    { "name": "Aircraft Carrier" , "length": 5 }  ,
    { "name": "Battleship"       , "length": 4 } ,
    { "name": "Cruiser"          , "length": 3 } ,
    { "name": "Submarine"        , "length": 3 } ,
    { "name": "Patrol Boat"      , "length": 2 } ,
];

Classic.prototype.BOARD_DIMS = {
    "rows": 10,
    "cols": 10,
};

Classic.prototype.wsReady = function () {
    if (this.ws.readyState != readyState.OPEN) {
        console.log("Websocket not ready: " + this.ws.readyState);
        return false;
    }
    return true;
}

Classic.prototype.sendLast = function() {
    if (!this.wsReady()) {
        return false;
    }
    this.ws.send(JSON.stringify(this.lastMsg));
    return true;
}

Classic.prototype.sendJoin = function (player_name) {
    if (!this.wsReady()) {
        return false;
    }

    var msg = {
        "type": "join_game",
        "player_name": player_name,
        "game_name": this.name,
    };

    this.lastMsg = msg;

    this.ws.send(JSON.stringify(msg));
    return true;
}

Classic.prototype.sendStart = function () {
    if (!this.wsReady()) {
        return false;
    }

    var msg = {
        "type": "start_game",
        "game_id": this.id,
        "player_id": this.playerId,
    };

    this.lastMsg = msg;

    this.ws.send(JSON.stringify(msg));
    return true;
}

Classic.prototype.sendPlaceShips = function() {
    if (!this.wsReady()) {
        return false;
    }

    var msg = {
        "type": "place_ships",
        "game_id": this.id,
        "player_id": this.playerId,
        "placements": [],
    };

    for (var i = 0; i < this.playerShips.length; ++i) {
        msg.placements.push({
            "ship_id": this.playerShips[i].ship_id,
            "direction": this.playerShips[i].direction,
            "location": {
                "row": this.playerShips[i].headRow,
                "col": this.playerShips[i].headCol,
            },
        });
    }

    this.lastMsg = msg;

    this.ws.send(JSON.stringify(msg));
    return true;
}

Classic.prototype.sendShot = function(row, col) {
    if (!this.wsReady()) {
        return false;
    }

    var msg = {
        "type": "make_shot",
        "game_id": this.id,
        "shootee_id": this.opponentId,
        "shooter_id": this.playerId,
        "location": {
            "row": row,
            "col": col,
        }
    };

    this.lastMsg = msg;

    this.ws.send(JSON.stringify(msg));
    return true;
}

Classic.prototype.shoot = function(row, col) {
    if (this.state !== "attack") {
        this.alertfail("Cannot make shot when not in attack phase",
            text.ok, noOp);
        return;
    }
    if (!this.turn) {
        this.alertfail("You can't shoot when it's not your turn",
            text.ok, noOp);
        return;
    }

    this.turn = false; // Your turn is over once you fire
    return this.sendShot(row, col);
}

Classic.prototype.finalizePlacements = function() {
    if (this.state !== "setup") {
        this.alertfail("Cannot place ships in state " + this.state,
            text.ok, noOp);
        return false;
    }

    for (var i = 0; i < this.playerShips.length; ++i) {
        // Fail if the ship hasn't been placed
        if (!this.playerShips[i].spots) return false;
    }

    var succ = this.sendPlaceShips();
    if (succ) {
        this.state = "playing";
    }

    return succ;
}

Classic.prototype.shipPlaced = function(shipIdx) {
    return this.playerShips[shipIdx].spots
        && (this.playerShips[shipIdx].spots.length ==
                this.playerShips[shipIdx].ship_length);
}

Classic.prototype.removeShip = function(shipIndex) {
    if (this.state !== "setup") {
        this.alertfail("Cannot remove ships outside of the setup phase",
            text.ok, noOp);
    }

    var shipId = this.playerShips[shipIndex].ship_id;

    this.playerShips[shipIndex].spots = null;
    this.playerShips.direction = null;
    this.playerShips.headRow = null;
    this.playerShips.headCol = null;

    for (var row = 0; row < this.board.length; ++row) {
        for (var col = 0; col < this.board[row].length; ++col) {
            if (this.board[row][col].id === shipId) {
                this.board[row][col].id = null;
                this.board[row][col].hit = null;
                this.board[row][col].color = null;
            }
        }
    }
}

Classic.prototype.placeShip = function(shipIndex, dir, row, col) {
    if (this.state !== "setup") {
        this.alertfail("Cannot place ships outside of the setup phase",
            text.ok, noOp);
        return false;
    }

    // Hard metadata for message
    this.playerShips[shipIndex].direction = dir;
    this.playerShips[shipIndex].headRow   = row;
    this.playerShips[shipIndex].headCol   = col;

    // Conveniently also removes the old placement of the ship (!)
    this.playerShips[shipIndex].spots = [];

    // Does the ship fit?
    var ship = this.playerShips[shipIndex];
    var fits = true;
    for (var i = 0; i < ship.ship_length; ++i) {
        switch (dir) {
            case "up":
                if (row < (ship.ship_length - 1)) fits = false;
                if (!!this.board[row - i][col].id) fits = false;
                break;
            case "down":
                if ((game.BOARD_DIMS.rows - ship.ship_length) < row)
                    fits = false;
                if (!!this.board[row + i][col].id) fits = false;
                break;
            case "left":
                if (col < (ship.ship_length - 1)) fits = false;
                if (!!this.board[row][col - i].id) fits = false;
                break;
            case "right":
                if ((game.BOARD_DIMS.cols - ship.ship_length) < col)
                    fits = false;
                if (!!this.board[row][col + i].id) fits = false;
                break;
            default:
                console.log("[Classic.placeShip]: Invalid placement "
                    + "direction: " + dir);
                fits = false;
                break;
        }
    }

    if (!fits) {
        console.log("Ship either does not fit or collides with another ship");
        return false;
    }

    // Commit to the board
    for (var i = 0; i < ship.ship_length; ++i) {
        switch (dir) {
            case "up":
                this.board[row - i][col] = { "id": ship.ship_id,
                    "hit": false,
                    "color": displayColors[shipIndex],
                };
                break;
            case "down":
                this.board[row + i][col] = { "id": ship.ship_id,
                    "hit": false,
                    "color": displayColors[shipIndex],
                };
                break;
            case "left":
                this.board[row][col - i] = { "id": ship.ship_id,
                    "hit": false,
                    "color": displayColors[shipIndex],
                };
                break;
            case "right":
                this.board[row][col + i] = { "id": ship.ship_id,
                    "hit": false,
                    "color": displayColors[shipIndex],
                };
                break;
            default:
                console.log("[Classic.placeShip]: Invalid placement "
                    + "direction: " + dir);
                return false;
        }

        this.playerShips[shipIndex].spots.push({
            "row": row,
            "col": col,
        });
    }

    return true;

}

Classic.prototype.start = function () {
    if (this.state === "setup") {
        // Game already started, so do nothing
        return;
    } else if (this.state !== "lobby") {
        this.alertfail("Cannot start game: Not in lobby",
            text.ok, noOp);
        return;
    }

    this.sendStart();
    this.state = "setup";
}

Classic.prototype.join = function (player_name) {

    var succ = this.sendJoin(player_name);

    if (succ) {
        this.state = "lobby";
        this.playerName = player_name;
        dontForgetMe(this.playerName);
        return true;
    } else {
        this.alertFail("Failed to join game " + this.name + " as " +
            player_name,
            text.tryAgain, fromTheTop);
        return false;
    }
}

Classic.prototype.connect = function (serverURI, onOpenCallback) {
    this.ws = new WebSocket(serverURI);

    this.ws.onopen = onOpenCallback;
    this.ws.onclose = logClose;
    this.ws.onerror = (function(event) {
        // Don't bother if the game's over
        if (this.state === "over") return;
        setupPopup("WebSocket error: " + event.type,
            text.newGame,
            setupNewGame,
            );
    }).bind(this);

    // This feels extra shitty to do
    this.ws.onmessage = this.delegate.bind(this);
};

Classic.prototype.alertfail = function(errormsg) {
    this.error = errormsg;
    console.log(this.error);
    if (arguments.length === 3) {
        setupPopup(arguments[0], arguments[1], arguments[2]);
    } else if (this.alert) {
        setupPopup(arguments[0], function(){}, function(){});
    }
}

Classic.prototype.alert = function(msg) {
    if (arguments.length === 3) {
        setupPopup(arguments[0], arguments[1], arguments[2]);
    } else if (this.alert) {
        setupPopup(arguments[0], function(){}, function(){});
    }
}

Classic.prototype.abort = function () {
    if (!this.wsReady()) {
        return false;
    }

    var msg = {
        "type": "abort_game",
        "game_id": this.id,
        "player_id": this.playerId,
        "reason": `${this.playerName} left the game`
    };

    this.lastMsg = msg;

    this.ws.send(JSON.stringify(msg));
    return true;
}

Classic.prototype.disconnect = function() {
    if (this.wsReady()) {
        this.ws.close();
    }
}

Classic.prototype.receive = function(row, col, hitType) {
    this.board[row][col].hit = hitType;
}

Classic.prototype.resultOfShot = function(row, col, res) {
    this.tracking[row][col] = !!res;
}

function logClose(event) {
    // Service restart -> server is going down
    if (event.code === 1012 || event.code === 1000) {
        setupPopup(`WebSocket closed: ${event.reason}`,
            text.ok, noOp,
            text.newGame, setupNewGame);
    } else {
        setupPopup("WebSocket closed unexpectedly: "
            + `${event.code} ${event.reason}`,
            text.ok, noOp,
            text.newGame, setupNewGame);
    }
    console.log("Websocket connection closed " + (event.wasClean ? "" : "un")
        + "cleanly (" + event.code + ") because " + event.reason);
}

Classic.prototype.delegate = function(event) {
    // Fail if in an error state
    if (this.error) {
        this.alertfail(this.error, text.ok, noOp);
        return;
    }

    var msg = event.data;
    this.process(JSON.parse(msg));
}

// Expecting res as an object
Classic.prototype.process = function(res) {
    var status = res.status;
    var reason = res.reason;
    if (!status) {
        // Restore turn if failed while making a shot
        if (this.lastMsg.type === "make_shot") {
            this.turn = !this.turn;
        }
        setupPopup("Failure: " + reason,
            text.ok, setupByGameState,
            text.ignore, function(){});
    }

    switch (res.type) {
        case "update":
            this.update(res);
            break;
        case "join_game":
            if (this.playerId) {
                // Do nothing if we've already gotten a response
                return;
            }

            this.playerId = res.player_id;
            this.id = res.game_id;
            this.playerShips = res.ships;

            if (res.opponents.length === 1) {
                this.opponentName = res.opponents[0].name;
                this.opponentId = res.opponents[0].id;
                this.opponentShips = res.opponents[0].ships;

                newChallenger(this.opponentName);

            } else if (res.opponents.length != 0) {
                var errorstr = "Illegal number of opponents: " +
                    res.opponents.length + " (maximum 1)"
                setupPlayPhase("Error joining game: " + errorstr,
                    text.ok, setupNewGame);
                return;
            }
            console.log("Joined game " + this.name + "(" + this.id + ")");
            break;
        case "start_game":
            if (!res.status) {
                setupPopup("Failed to ready up: " + res.reason);

                // RIP
                $("#start-button").text(text.go).on("click", function() {
                    $("#start-button").text(text.ready).off("click");
                    game.start();
                });
            }
            break;
        case "place_ships":
            var failures = [];
            for (var i = 0; i < res.results.length; ++i) {
                if (!res.results[i].status) {
                    failures.push(res.results[i]);
                }
            }

            if (failures.length > 0) {
                var errStr = "Error placing the following ships: ";
                var sep = "";
                var failedShips = this.playerShips.filter(
                    ship => ship.ship_id === failures[i].ship_id
                );

                for (var i = 0; i < failures.length; ++i) {
                    errStr += sep + failedShips[i];
                    sep = ", ";
                    this.removeShip(failedShips[i].ship_id);
                    removeShipFromBoard(failedShips[i].ship_id);
                }

                this.alertFail(errStr, text.tryAgain, setupSetupPhase);
            }
            break;
        case "make_shot":
            var outcome = res.result;
            var row = res.location.row;
            var col = res.location.col;
            if (outcome === "miss") {
                updateTile("#t" + row + col, "miss");
                this.resultOfShot(row, col, false);
            } else if (outcome === "hit" || outcome === "sink") {
                updateTile("#t" + row + col, "hit");
                this.resultOfShot(row, col, true);

                if (outcome === "sink") {
                    this.resultOfShot(row, col, true);
                    var sunk = this.opponentShips.find(function(s) {
                        return s.ship_id === res.ship_id;
                    });
                    sunk.sunk = true;
                    killShip(res.ship_id);
                    setupPopup("You sunk " + this.opponentName + "'s "
                        + sunk.ship_name,
                        text.ok, noOp);
                }
            }
            setMainDisplayBoard();
            break;
    }
}

Classic.prototype.update = function(pkt) {
    switch (pkt.update_type) {
        case "opponent_joined":
            if (this.opponentId) {
                // Do nothing if we already know about our opponent
                return;
            }

            this.opponentName = pkt.opponent.name;
            this.opponentId = pkt.opponent.id;
            this.opponentShips = pkt.opponent.ships;

            newChallenger(this.opponentName);

            break;
        case "start_game":
            if (this.turn !== null) {
                // Do nothing if we've already started the game
                return;
            }
            this.turn = ((pkt.first_turn === this.playerId) ? true : false);
            this.state = "setup";
            // Fuck
            setupSetupPhase(false); // <-- Jank
            break;
        case "start_attack":
            this.state = "attack";
            setupPlayPhase();
            break;
        case "receive_shot":
            var row = pkt.location.row;
            var col = pkt.location.col;
            var outcome = pkt.result;
            if (outcome === "miss") {
                updateTile("#b" + row + col, "miss");
                this.receive(row, col, false);
            } else if (outcome === "hit" || outcome === "sink") {
                updateTile("#b" + row + col, "hit");
                this.receive(row, col, true);
                if (outcome === "sink") {
                    var sunk = this.playerShips.find(function(e) {
                        return e.ship_id === pkt.ship_id;
                    });
                    sunk.sunk = true;
                    killShip(pkt.ship_id);
                    setupPopup(this.opponentName + " sank your "
                        + sunk.ship_name,
                        text.ok, noOp);
                }
            }
            this.turn = true; // It is your turn once you've been shot
            setMainDisplayTracking();
            break;
        case "game_end":
            if (this.state != "attack") {
                return; // Normal endgame will only be from this state
            }
            this.state = "over";
            this.turn = false;
            var winner = pkt.winner;
            if (winner === this.playerId) {
                setupPopup(text.victory,
                    text.again, setupNewGame,
                    text.ok, noOp);
            } else {
                setupPopup(text.defeat,
                    text.again, setupNewGame,
                    text.ok, noOp)
            }
            this.disconnect();
            break;
        case "abort_game":
            pid = pkt.player_id;
            offender = this.opponentName;

            if (pid === this.playerId) {
                offender = this.playerName;
            }

            // One of these days, remove this.
            var disconnectFirst = function(callback) {
                return function() {
                    this.disconnect();
                    callback();
                }.bind(this);
            }.bind(this);

            this.disconnect();

            setupPopup(`Game aborted by ${offender} because ${pkt.reason}`,
                text.ok, disconnectFirst(noOp),
                text.again, disconnectFirst(setupNewGame));
            break;
        case "repeat_that":
            this.sendLast();
            break;
    }
}

