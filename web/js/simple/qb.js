
"use strict";

// Should be colorblind friendly
var displayColors = [
    "#B35806",
    "#DF8214",
    "#B2ABD2",
    "#8073AC",
    "#541D88",
];

// var serverURI = "ws://localhost:4680";
var serverURI = "wss://questionablebattleship.com:4680"

var text = {
    "title": "Electric Boogaloo",
    "go": "Go!",
    "continue": "Continue",
    "cancel": "Cancel",
    "prompt_player_name": "Who are you?",
    "prompt_room_name": "Where are you going?",
    "ok": "Okay",
    "ignore": "Ignore",
    "victory": "Victory",
    "defeat": "Defeat",
    "again": "Play more",
    "ships": "Ships",
    "newGame": "New Game",
    "tryAgain": "Try Again",
    "done": "Done",
    "ready": "Ready",
};

var game = null;
var baseURI = "";

$(document).ready(function() {

    baseURI = window.location.href;

    var trailing = baseURI.charAt(baseURI.length - 1);
    if (trailing !== '/') {
        baseURI += '/';
    }

    // Attempt to safely close
    window.onbeforeunload = function() {
        console.log("Aborting game and closing connection");
        disengageAndAbort();
    }

    shuffleColors();
    setupHotkeys();
    // Ensure that this function matches the initial state of the webpage.
    setupNewGame();
});

function setupNewGame() {
    if (game) game.disconnect();
    game = null;

    fromTheTop();

    setupJoinPhase();
}

function fromTheTop() {
    var g = $("#game").empty();

    $("<p></p>", { "id": "title" })
        .text(text.title)
        .appendTo(g);

    $("<p></p>")
        .text(text.prompt_player_name)
        .appendTo(g);
    $("<input></input>", { "id": "player-name" })
        .appendTo(g);

    $("<p></p>")
        .text(text.prompt_room_name)
        .appendTo(g);
    $("<input></input>", { "id": "room-name" })
        .appendTo(g);

    var buttons = $("<div></div>")
        .addClass("buttons")
        .appendTo(g);

    $("<button></button>", {
        "id": "join-room",
        "type": "button",
        "name": "join"})
        .text(text.go)
        .appendTo(buttons);

    // Back to nothing, baby
    window.history.pushState(null, "Join a game", baseURI);
}

function setupHotkeys() {
    $(document).bind("keydown", function (e) {
        if (e.defaultPrevented) {
            return; // Do nothing if prevented
        }
        var handled = false;

        e = e || window.event;
        var charCode = e.which || e.keyCode;
        switch (charCode) {
            case 13: // Enter/Return
                $(".hotkey-enter").click();
                break;
            case 27: // Esc
                $(".hotkey-esc").click();
        }

        if (handled) {
            e.preventDefault();
        }
    });
}

function setupJoinPhase() {
    $("#join-room").click(join_game);
}

function join_game() {
    var gname = $("#room-name").val();
    var pname = $("#player-name").val();
    game = new Classic(gname);
    game.connect(serverURI, function() {
        setupLobbyPhase();

        if (game.join(pname)) {
            console.log(game.playerName + " joined the game");
        } else {
            console.log("Failed to join")
        }
    });
}

function setupLobbyPhase() {
    var g = $("#game");
    g.empty();

    // Lay out the lobby

    // Title in the corner
    $("<p></p>", { "id": "top-left" })
        .addClass("heading")
        .text(text.title)
        .appendTo(g);

    // Room label
    $("<p></p>", { "id": "room" })
        .text(game.name)
        .appendTo(g);

    // Player list
    $("<div></div>")
        .addClass("player-list")
        .addClass("container")
        .appendTo(g);

    var plist = $(".player-list");

    // You
    var you = $("<div></div>", { "id": "you" })
        .addClass("box player-mugshot")
        .text(game.player_name + " (You)");

    // Opponent
    var not_you = $("<div></div>", { "id": "opponent" })
        .addClass("box player-mugshot")
        .text("Waiting for opponent");

    var stretch = $("<span></span>")
        .addClass("stretch");

    plist.append(you)
        .append(__space())
        .append(not_you)
        .append(__space())
        .append(stretch);

    // Buttons
    var buttons = $("<div></div>")
        .addClass("buttons")
        .appendTo(g);

    // Go
    $("<button></button>", { "id": "start-button" })
        .text(text.go)
        .click(function() {
            $("#start-button").text(text.ready).off("click");
            game.start();
        })
        .appendTo(buttons);

    // Uhh
    window.history.pushState({
        "name": game.playerName,
        "room": game.name,
    }, "Room" + game.name, baseURI + game.name);

}

function newChallenger(name) {
    $("#opponent").text(name);
}

function dontForgetMe(name) {
    $("#you").text(name + " (You)");
}

// Pass false as first arg to stop game sending start event
function setupSetupPhase() {
    var g = $("#game");
    g.empty();

    // Title in the corner
    $("<p></p>", { "id": "top-left" })
        .addClass("heading")
        .text(text.title)
        .appendTo(g);

    // Game name
    $("<div></div>")
        .append($("<p></p>").text(game.name))
        .appendTo(g);

    // Gameplay area
    var g_ = $("<div></div>", { "id": "not-title" })
        .appendTo(g);

    // Sidebar
    var sidebar = $("<div></div>", { "id": "sidebar" })
        .appendTo(g_);

    // Ship label
    $("<p></p>")
        .text(text.ships)
        .addClass("heading")
        .appendTo(sidebar);

    // Maybe it's time to abuse function closures...
    var placementInfo = {
        "direction": "up", // One of up, down, left, right
        "ship": 0, // Index into gmae.playerShips
    };

    // Ship list
    var ships = $("<div></div>", { "id": "ship-list" })
        .appendTo(sidebar);

    for (var i = 0; i < game.SHIP_INFO.length; ++i) {
        var color = displayColors[i];
        $("<p></p>", { "id": game.playerShips[i].ship_id })
            .text(game.playerShips[i].ship_name
                + " ("
                + game.playerShips[i].ship_length
                + ")")
            .addClass("inactive")
            .css({
                "border-top": "3px dashed " + color,
                "border-bottom": "3px dashed " + color,
                "border-right": "3px dashed " + color,
                "border-left": "3px dashed " + color,
                "padding": "0.3em",
            })
            .click(function(idx) {
                return function() {
                    placementInfo.ship = idx;
                    selectShip(game.playerShips[idx].ship_id);
                }
            }(i))
            .appendTo(ships);
    }

    // Ship 0 active by default
    selectShip(game.playerShips[placementInfo.ship].ship_id);

    // Side bottom thing
    $("<div></div>", { "id": "sidebar-bottom" })
        .text(text.done)
        .click(function () {
            if (game.finalizePlacements()) {
                $("#sidebar-bottom")
                    .empty()
                    .append($("<img></img>", {
                        "src": "/images/loading/Eclipse.gif",
                        "alt": "Loading icon (Waiting for other player...)",
                        "height": $("#sidebar-bottom").width(), // Jank
                        "width": $("#sidebar-bottom").width(), // Jank
                        "z-index": "999",
                    }))
                    .off("click");
            } else {
                game.alertFail("You need to place all of your ships!",
                    text.ok, noOp);
                console.log("Not all ships placed");
            }
        })
        .appendTo(sidebar);

    // Main board
    var main_display = $("<div></div>", { "id": "canvas" })
        .appendTo(g_);

    // 10 by 10
    var spaces = [];
    var board = $("<div></div>", { "id": "board" })
        .appendTo(main_display);

    for (var row = 0; row < game.BOARD_DIMS.rows; ++row) {

        var row_ = $("<div></div>")
            .addClass("row")
            .appendTo(board);

        for (var col = 0; col < game.BOARD_DIMS.cols; ++col) {

            var s = $("<div></div>", { "id": String(row) + String(col) })
                .addClass("spot")
                .click((function (row, col) {
                    return function () {
                        // Capture placementInfo from outside the loop
                        placementInfo.ship =
                            placeShip(placementInfo, row, col);
                    };
                })(row, col))
                // Bind mouseover and mouseout
                .mouseover(function(row, col) {
                    return function() {
                        // Capture placementInfo from outside the loop
                        previewShip(placementInfo, row, col);
                    };
                }(row, col))
                .mouseout(function(row, col) {
                    return function() {
                        unpreviewShip();
                    };
                }(row, col))
                // Bind right-click to change directions
                .contextmenu(function(row, col) {
                    return function() {
                        switch (placementInfo.direction) {
                            case "up":
                                placementInfo.direction = "right";
                                break;
                            case "down":
                                placementInfo.direction = "left";
                                break;
                            case "left":
                                placementInfo.direction = "up";
                                break;
                            case "right":
                                placementInfo.direction = "down";
                                break;
                            default:
                                console.log("[previewShip]: Invalid "
                                    + "placement direction: " + dir);
                                break;
                        }
                        unpreviewShip();
                        previewShip(placementInfo, row, col);
                        return false;
                    };
                }(row, col))
                .appendTo(row_);

            if (row === 0) {
                s.addClass("top");
            }

            if (col === 0) {
                s.addClass("left");
            }
        }
    }

    if (arguments.length == 1 && arguments[0]) {
        game.start();
    } else { // Begin jank
        game.state = "setup";
    }
}

function selectShip(id) {
    unselectAllShips();

    $("#" + id).removeClass("inactive").addClass("selected");
}

function unselectAllShips() {
    var ships = $("#ship-list").children();
    ships.removeClass("selected")
         .removeClass("inactive")
         .addClass("inactive");
}

function killShip(id) {
    $("#" + id).removeClass("alive").addClass("dead");
}

function reviveShip(id) {
    $("#" + id).addClass("alive").removeClass("dead");
}

function removeShip(id) {
    game.removeShip(id);
}

function removeShipFromBoard(ship_id) {
    // Remove any bits of the ship that are already on the board
    $("." + ship_id)
        .removeClass(ship_id)
        .removeClass("ship")
        .css("background-color", "");
}

function placeShip(placementInfo, row, col) {
    if (placementInfo.ship === -1) {
        // Do nothing if no ship selected
        return -1;
    }
    var ship = game.playerShips[placementInfo.ship];

    var id = ship.ship_id;
    var length = ship.ship_length;

    var dir = placementInfo.direction;

    var color = displayColors[placementInfo.ship];

    // Select this ship
    selectShip(id);
    game.removeShip(placementInfo.ship);

    // Does the ship fit?
    // Bounds/Collisions
    var fits = true;
    switch (dir) {
        case "up":
            if (row < (ship.ship_length - 1)) fits = false;
            for (var i = 0; i < ship.ship_length; ++i) {
                var spot = $("#" + String(row - i) + String(col));
                if (spot.hasClass("ship")) fits = false;
            }
            break;
        case "down":
            if ((game.BOARD_DIMS.rows - ship.ship_length) < row) fits = false;
            for (var i = 0; i < ship.ship_length; ++i) {
                var spot = $("#" + String(row + i) + String(col));
                if (spot.hasClass("ship")) fits = false;
            }
            break;
        case "left":
            if (col < (ship.ship_length - 1)) fits = false;
            for (var i = 0; i < ship.ship_length; ++i) {
                var spot = $("#" + String(row) + String(col - i));
                if (spot.hasClass("ship")) fits = false;
            }
            break;
        case "right":
            if ((game.BOARD_DIMS.cols - ship.ship_length) < col) fits = false;
            for (var i = 0; i < ship.ship_length; ++i) {
                var spot = $("#" + String(row) + String(col + i));
                if (spot.hasClass("ship")) fits = false;
            }
            break;
        default:
            console.log("[previewShip]: Invalid placement direction: " + dir);
            break;
    }

    if (!fits) {
        return placementInfo.ship;
    }

    // Remove any bits of the ship that are already on the board
    $("." + id)
        .removeClass(id)
        .removeClass("ship")
        .css("background-color", "");

    // Place the ship
    var allowed = false;
    switch (dir) {
        case "up":
            if (row < (ship.ship_length - 1)) return;
            for (var i = 0; i < ship.ship_length; ++i) {
                $("#" + String(row - i) + String(col))
                    .addClass(id)
                    .addClass("ship")
                    .removeClass("preview")
                    .css("background-color", color);
            }
            allowed = game.placeShip(placementInfo.ship, dir, row, col);
            break;
        case "down":
            if ((game.BOARD_DIMS.rows - ship.ship_length) < row) return;
            for (var i = 0; i < ship.ship_length; ++i) {
                $("#" + String(row + i) + String(col))
                    .addClass(id)
                    .addClass("ship")
                    .removeClass("preview")
                    .css("background-color", color);
            }
            allowed = game.placeShip(placementInfo.ship, dir, row, col);
            break;
        case "left":
            if (col < (ship.ship_length - 1)) return;
            for (var i = 0; i < ship.ship_length; ++i) {
                $("#" + String(row) + String(col - i))
                    .addClass(id)
                    .addClass("ship")
                    .removeClass("preview")
                    .css("background-color", color);
            }
            allowed = game.placeShip(placementInfo.ship, dir, row, col);
            break;
        case "right":
            if ((game.BOARD_DIMS.cols - ship.ship_length) < col) return;
            for (var i = 0; i < ship.ship_length; ++i) {
                $("#" + String(row) + String(col + i))
                    .addClass(id)
                    .addClass("ship")
                    .removeClass("preview")
                    .css("background-color", color);
            }
            allowed = game.placeShip(placementInfo.ship, dir, row, col);
            break;
        default:
            console.log("[placeShip]: Invalid placement direction: " + dir);
            break;
    }

    // Begin jank
    if (!allowed) {
        // Remove any bits of the ship that are already on the board
        $("." + id).removeClass(id)
            .removeClass("ship")
            .css("background-color", "");
    }

    // Find the next ship that hasn't been placed
    // If all ships placed, return -1.
    for (var i = 1; i < game.playerShips.length; ++i) {
        var idx = (placementInfo.ship + i) % game.playerShips.length
        if (!game.shipPlaced(idx)) {
            var id_ = game.playerShips[idx].ship_id;
            // Select next ship
            selectShip(id_);
            return idx;
        }
    }

    unselectAllShips();
    return -1;
}

// Attempt to display a preview. Do nothing if the ship doesn't fit
function previewShip(placementInfo, row, col) {
    if (typeof(placementInfo.ship) === "undefined"
        || placementInfo.ship === null) {
        return; // ಠ_ಠ
    }
    if (placementInfo.ship < 0 || placementInfo >= game.playerShips.length) {
        // Do nothing if not in range
        return;
    }

    var ship = game.playerShips[placementInfo.ship];
    var dir = placementInfo.direction;

    var color = displayColors[placementInfo.ship];

    var muchRed = "#FF4747";

    var fits = true;

    // Does the ship fit?
    // Bounds/Collisions
    switch (dir) {
        case "up":
            if (row < (ship.ship_length - 1)) fits = false;
            for (var i = 0; i < ship.ship_length; ++i) {
                var spot = $("#" + String(row - i) + String(col));
                if (spot.hasClass("ship")) fits = false;
            }
            break;
        case "down":
            if ((game.BOARD_DIMS.rows - ship.ship_length) < row) fits = false;
            for (var i = 0; i < ship.ship_length; ++i) {
                var spot = $("#" + String(row + i) + String(col));
                if (spot.hasClass("ship")) fits = false;
            }
            break;
        case "left":
            if (col < (ship.ship_length - 1)) fits = false;
            for (var i = 0; i < ship.ship_length; ++i) {
                var spot = $("#" + String(row) + String(col - i));
                if (spot.hasClass("ship")) fits = false;
            }
            break;
        case "right":
            if ((game.BOARD_DIMS.cols - ship.ship_length) < col) fits = false;
            for (var i = 0; i < ship.ship_length; ++i) {
                var spot = $("#" + String(row) + String(col + i));
                if (spot.hasClass("ship")) fits = false;
            }
            break;
        default:
            console.log("[previewShip]: Invalid placement direction: " + dir);
            break;
    }

    var disColor = fits ? color : muchRed;

    // Highlight the ship
    switch (dir) {
        case "up":
            for (var i = 0; i < ship.ship_length; ++i) {
                if ((row - i) < 0) break;
                var spot = $("#" + String(row - i) + String(col));
                if (spot.hasClass("ship")) continue;
                spot.addClass("preview")
                    .css({
                        "background-color": disColor,
                    });
            }
            break;
        case "down":
            for (var i = 0; i < ship.ship_length; ++i) {
                if ((row + i) > (game.BOARD_DIMS.rows - 1)) break;
                var spot = $("#" + String(row + i) + String(col));
                if (spot.hasClass("ship")) continue;
                spot.addClass("preview")
                    .css({
                        "background-color": disColor,
                    });
            }
            break;
        case "left":
            for (var i = 0; i < ship.ship_length; ++i) {
                if ((col - i) < 0) break;
                var spot = $("#" + String(row) + String(col - i));
                if (spot.hasClass("ship")) continue;
                spot.addClass("preview")
                    .css({
                        "background-color": disColor,
                    });
            }
            break;
        case "right":
            for (var i = 0; i < ship.ship_length; ++i) {
                if ((col + i) > (game.BOARD_DIMS.cols - 1)) break;
                var spot = $("#" + String(row) + String(col + i));
                if (spot.hasClass("ship")) continue;
                spot.addClass("preview")
                    .css({
                        "background-color": disColor,
                    });
            }
            break;
        default:
            console.log("[previewShip]: Invalid placement direction: " + dir);
            break;
    }
}

function unpreviewShip() {
    // Clear previews
    $(".preview").css("background-color", "");
}

function setupPlayPhase() {
    // Empty the sidebar and board
    $("#sidebar").empty();
    $("#board").empty();

    if (game.turn) {
        setMainDisplayTracking();
    } else {
        setMainDisplayBoard();
    }
}

// Hmm...
// function setupPopup(msg, ...) {
// Arguments other than the first are interpreted as a pair (label, callback)
function setupPopup(msg) {
    var cleanupPopup = function(continuation) {
        return function() {
            $("#popup-wrapper").remove();
            continuation();
        }
    };

    // Get if exists, otherwise create
    var pWrap = $("#popup-wrapper");
    if (pWrap.length === 0) {
        pWrap = $("<div></div>", { "id": "popup-wrapper" });
    }

    // Get if exists, otherwise create
    var alignHelper = $("#popup-wrapper .align-helper");
    if (alignHelper.length === 0) {
        alignHelper = $("<div></div>").addClass("align-helper");
        alignHelper.appendTo(pWrap);
    }

    // No popup - create a new one
    var popup = $("#popup-msg");
    if (popup.length === 0) {
        popup = $("<div></div>", { "id": "popup-msg"})
            .text(msg)
            .append($("<br/>"))
            .appendTo(pWrap);
    }

    // Popup exists - append to it instead
    else {
        popup.append($("<br/>"))
            .append($("<p></p>")
                .text(msg));
    }

    for (var i = 1; i < arguments.length; i += 2) {
        $("<button></button>")
            .text(arguments[i])
            .click(cleanupPopup(arguments[i + 1]))
            .appendTo(popup);
    }

    $("body").append(pWrap);
}

function setMainDisplayTo(target) {
    if (target === "board") {
        setMainDisplayBoard();
    } else if (target === "tracking") {
        setMainDisplayTracking();
    } else {
        console.log(target + " is not a valid target for the main display");
        return;
    }
}

function setMainDisplayTracking() {
    // Sidebar
    var sidebar = $("#sidebar").empty();

    // Sidebar ship list
    var shipList = $("<div></div>", { "id": "ship-list" })
        .text(`${game.opponentName}'s ` + text.ships)
        .addClass("heading")
        .appendTo(sidebar);

    for (var i = 0; i < game.opponentShips.length; ++i) {
        var ship = game.opponentShips[i];
        var label = $("<p></p>", { "id": ship.ship_id })
            .text(ship.ship_name + ` (${ship.ship_length})`)
            .addClass("dashed-border")
            .appendTo(shipList);

        reviveShip(ship.ship_id);
        if (game.opponentShips[i].sunk) {
            killShip(ship.ship_id);
        }
    }

    // Side bottom thing
    $("<div></div>", { "id": "sidebar-bottom" })
        .click(function() {
            setMainDisplayTo("board");
        })
        .appendTo(sidebar);

    // Populate the sidebar bottom
    var board = $("#sidebar-bottom").empty();
    for (var row = 0; row < game.BOARD_DIMS.rows; ++row) {
        var row_ = $("<div></div>")
            .addClass("row")
            .appendTo(board);
        for (var col = 0; col < game.BOARD_DIMS.cols; ++col) {
            var s = $("<div></div>", { "id": "b" + String(row) + String(col) })
                .addClass("spot")
                .appendTo(row_);
            if (row === 0) { s.addClass("top"); }
            if (col === 0) { s.addClass("left"); }
            if (!!game.board[row][col].id) {
                s.addClass("ship")
                    .addClass(game.board[row][col].id)
                    .css({
                        "background-color": game.board[row][col].color,
                    });
            }
            if (game.board[row][col].hit !== null) {
                if (game.board[row][col].hit) {
                    s.addClass("hit");
                } else if (!game.board[row][col].hit && !s.hasClass("ship")) {
                    s.addClass("miss");
                }
            }
        }
        if (row === game.BOARD_DIMS.rows - 1) { row_.addClass("bottom"); }
    }

    // 10 by 10 board
    var tracking = $("#board").empty();
    for (var row = 0; row < game.BOARD_DIMS.rows; ++row) {
        var row_ = $("<div></div>")
            .addClass("row")
            .appendTo(tracking);
        for (var col = 0; col < game.BOARD_DIMS.cols; ++col) {
            var s = $("<div></div>", { "id": "t" + String(row) + String(col) })
                .addClass("spot")
                .addClass("tracking")
                .click(function(row, col) {
                    return function() {
                        if (game.over) return; // Jank
                        if (!game.turn) {
                            game.alert("You can't shoot when it's "
                                + "not your turn");
                            return;
                        }
                        var cell = $(`#t${row}${col}`);
                        if (!cell.hasClass("hit") && !cell.hasClass("miss")) {
                            game.shoot(row, col);
                        }
                    }
                }(row, col))
                .appendTo(row_);
            if (row === 0) { s.addClass("top"); }
            if (col === 0) { s.addClass("left"); }
            if (game.tracking[row][col] !== null) {
                if (game.tracking[row][col]) {
                    s.addClass("hit");
                } else {
                    s.addClass("miss");
                }
            }
        }
    }
}

function setMainDisplayBoard() {
    // Sidebar
    var sidebar = $("#sidebar").empty();

    // Sidebar ship list
    var shipList = $("<div></div>", { "id": "ship-list" })
        .text(`Your ` + text.ships)
        .addClass("heading")
        .appendTo(sidebar);

    for (var i = 0; i < game.playerShips.length; ++i) {
        var ship = game.playerShips[i];
        var label = $("<p></p>", { "id": ship.ship_id })
            .text(ship.ship_name + ` (${ship.ship_length})`)
            .addClass("dashed-border")
            .appendTo(shipList);

        reviveShip(ship.ship_id);
        if (game.playerShips[i].sunk) {
            killShip(ship.ship_id);
        }
    }

    // Side bottom thing
    $("<div></div>", { "id": "sidebar-bottom" })
        .click(function() {
            setMainDisplayTo("tracking");
        })
        .appendTo(sidebar);

    // Populate the sidebar bottom
    var board = $("#board").empty();
    for (var row = 0; row < game.BOARD_DIMS.rows; ++row) {
        var row_ = $("<div></div>")
            .addClass("row")
            .appendTo(board);
        for (var col = 0; col < game.BOARD_DIMS.cols; ++col) {
            var s = $("<div></div>", { "id": "b" + String(row) + String(col) })
                .addClass("spot")
                .addClass("tracking")
                .appendTo(row_);
            if (row === 0) { s.addClass("top"); }
            if (col === 0) { s.addClass("left"); }
            if (!!game.board[row][col].id) {
                s.addClass("ship")
                    .addClass(game.board[row][col].id)
                    .css({
                        "background-color": game.board[row][col].color,
                    });
            }
            if (game.board[row][col].hit !== null) {
                if (game.board[row][col].hit) {
                    s.addClass("hit");
                } else if (!game.board[row][col].hit && !s.hasClass("ship")) {
                    s.addClass("miss");
                }
            }
        }
    }

    // 10 by 10 board
    var tracking = $("#sidebar-bottom").empty();
    for (var row = 0; row < game.BOARD_DIMS.rows; ++row) {
        var row_ = $("<div></div>")
            .addClass("row")
            .appendTo(tracking);
        for (var col = 0; col < game.BOARD_DIMS.cols; ++col) {
            var s = $("<div></div>", { "id": "t" + String(row) + String(col) })
                .addClass("spot")
                .appendTo(row_);
            if (row === 0) { s.addClass("top"); }
            if (col === 0) { s.addClass("left"); }
            if (game.tracking[row][col] !== null) {
                if (game.tracking[row][col]) {
                    s.addClass("hit");
                } else {
                    s.addClass("miss");
                }
            }
        }
        if (row === game.BOARD_DIMS.rows - 1) { row_.addClass("bottom"); }
    }
}

function setupByGameState() {
    switch (game.state) {
        case "setup":
            setupSetupPhase();
            break;
        case "attack":
            setupPlayPhase();
            break;
        case "playing":
            setupPlayPhase();
            break;
        case "lobby":
            setupLobbyPhase();
            break;
        case "over":
            setupNewGame();
            break;
    }
}

function __text(text) {
    return document.createTextNode(text);
}

function __space() {
    return __text("\n");
}

// Dev function wheee
function disengageAndAbort() {
    if (game) {
        game.abort();
        game.disconnect();
    }
}

function shuffleColors() {
    displayColors = shuffleArray(displayColors);
}

function updateTile(selector, class_) {
    $(selector).addClass(class_);
}

/**
 * Randomize array element order in-place.
 * Using Durstenfeld shuffle algorithm.
 */
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}
